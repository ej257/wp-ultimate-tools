<?php if ( ! defined( 'WPINC' ) ) { die( "Don't mess with us." ); }
/**
 * Admin side of addon
 *
 * @since      1.0.0
 * @package    WP_Ultimate_Tools
 * @author     Goran Zivkovic
 */

if( !class_exists( 'WPUT_BLOCK_ADMIN' ) ) :
class WPUT_BLOCK_ADMIN extends WPUT_BLOCK_GLOBAL
{
    /**
     * Class constructor
     */
    function __construct()
    {
        /**
         * Actions
         */
        $this->add_action( 'admin_init', 'settings_init' );
        $this->add_action( WPUT_HOOK . 'settings', 'settings_admin_block_content' );
        $this->add_action( WPUT_HOOK . 'settings_tabs', 'settings_admin_block_tabs' );
        /**
         * Filters
         */
    }

    /**
     * Search options and settings
     */
    public function settings_init()
    {
        /**
         * Register settings
         * 
         * register_setting($option_group, $option_name, $sanitize_callback)
         */
        register_setting(
            WPUT_BLOCK_NAME . '-settings',
            WPUT_BLOCK_OPTIONS,
            array(
                'default'   => $this->default_options_admin_block
            )
        );

        /**
         * Add general section
         * 
         * add_settings_section($id, $title, $callback, $page)
         */
        add_settings_section(
            WPUT_BLOCK_NAME . '-general-options',
            apply_filters( WPUT_BLOCK_HOOK . 'general_settings_title', __( 'General Options', WPUT_BLOCK_NAME ) ),
            array( &$this, 'wput_admin_block_general_section' ),
            WPUT_BLOCK_NAME . '-page-settings'
        );

        /**
         * Add settings fields to section
         * 
         * add_settings_field($id, $title, $callback, $page, $section, $args);
         */

        add_settings_field(
            'block_roles',
            apply_filters( WPUT_BLOCK_HOOK . 'block_roles_title', __( 'Block roles to access admin side', WPUT_BLOCK_NAME ) ),
            array( &$this, 'block_roles' ),
            WPUT_BLOCK_NAME . '-page-settings',
            WPUT_BLOCK_NAME . '-general-options'
        );
        

        /**
         * Add settings fields to section
         * 
         * add_settings_field($id, $title, $callback, $page, $section, $args);
         */
        add_settings_field(
            'block_users',
            apply_filters( WPUT_BLOCK_HOOK . 'block_users_title', __( 'Block users access admin side', WPUT_BLOCK_NAME ) ),
            array( &$this, 'block_users' ),
            WPUT_BLOCK_NAME . '-page-settings',
            WPUT_BLOCK_NAME . '-general-options'
        );

    }


    /**
     * Show login options tab in WPUT settings
     */
    public function settings_admin_block_tabs()
    {
        $wput_block_admin_active = '';
        if( $this->is_settings_active( WPUT_BLOCK_NAME ) ) $wput_block_admin_active = ' nav-tab-active';
        ?>
        <a href="<?php echo self_admin_url( 'admin.php?page=' . $_GET['page'] . '&tab=' . WPUT_BLOCK_NAME ); ?>" class="nav-tab <?php echo $wput_block_admin_active; ?>"><?php _e( 'Admin Block', WPUT_BLOCK_NAME ); ?></a>
        <?php
    }

    /**
     * Show settings page in WPUT settings
     */
    public function settings_admin_block_content()
    {
        if( $this->is_settings_active( WPUT_BLOCK_NAME ) && file_exists( WPUT_BLOCK_ADMIN . '/settings.php' ) ) include WPUT_BLOCK_ADMIN . '/settings.php';
    }

    /**
     * Section callback
     */
    public function wput_admin_block_general_section( $args )
    {
        return false;
    }

    /**
     * Blocked roles option callback
     */
    public function block_roles()
    {
        $options = $GLOBALS['WPUT_ADMIN_BLOCK'];
        $roles = wp_roles();

        if( isset( $roles->role_names['administrator'] ) ) unset( $roles->role_names['administrator'] );
        if( !empty( $roles ) )
        {
            ?>
            <input type="checkbox" name="select_all" id="select-all"><?php _e( 'Select All', WPUT_NAME ); ?><br /><br />
            <?php
            foreach( $roles->role_names as $name => $full_name )
            {
                $checked = '';
                if( isset( $options['roles'][ $name ] ) ) $checked = ' checked';
                printf( '<input type="checkbox" name="%s[roles][%s]" value="%s" %s> %s<br />', WPUT_BLOCK_OPTIONS, $name, $name, $checked, $full_name );
            }
            printf( '<p class="description">%s</p>', __( 'Users will be able to login in but prevented to access admin side, logged out and redirected to login.' ) );
        }
        else
        {
            _e( 'No roles except administrator', WPUT_BLOCK_NAME );
        }
    }

    /**
     * Blocked users option callback
     */
    public function block_users()
    {
        $options = $GLOBALS['WPUT_ADMIN_BLOCK'];
        $current_user = wp_get_current_user();
        $users = get_users( array( 'exclude' => $current_user->ID ) );

        if( !empty( $users ) )
        {
            ?>
            <input type="checkbox" name="select_all" id="select-all"><?php _e( 'Select All', WPUT_NAME ); ?><br /><br />
            <?php
            foreach( $users as $i => $user )
            {
                $checked = '';
                if( isset( $options['users'][ (int)$user->ID ] ) ) $checked = ' checked';
                printf( '<input type="checkbox" name="%s[users][%d]" value="%d" %s> %s<br />', WPUT_BLOCK_OPTIONS, $user->ID, $user->ID, $checked, $user->display_name );
            }
            printf( '<p class="description">%s</p>', __( 'Users will be able to login in but prevented to access admin side, logged out and redirected to login.' ) );
        }
        else
        {
            _e( 'No useres except you.', WPUT_BLOCK_NAME );
        }
    }

}
endif;