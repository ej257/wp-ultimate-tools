<?php if ( ! defined( 'WPINC' ) ) { die( "Don't mess with us." ); }
/**
 * Admin side of addon
 *
 * @since      1.0.0
 * @package    WP_Ultimate_Tools
 * @author     Goran Zivkovic
 */

if( !class_exists( 'WPUT_BLOCK_BLOCKER' ) ) :
class WPUT_BLOCK_BLOCKER extends WPUT_BLOCK_GLOBAL
{
    /**
     * Class constructor
     */
    function __construct()
    {
        $this->add_action( 'wp_loaded', 'block_access', 1, 0 );
    }

    /**
     * Block user to login an to access admin side
     */
    public function block_access()
    {
        $actual_link = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
		if (strpos($actual_link, 'action=logout') !== false || strpos($actual_link, 'admin-ajax') !== false || defined('DOING_AJAX') && DOING_AJAX) {
		
			/* DO NOTHING */
			
        } 
        else 
        {
			if( is_user_logged_in() )
			{
                $options = $GLOBALS['WPUT_ADMIN_BLOCK'];
				$user = wp_get_current_user();
				if( isset( $options['users'][ (int)$user->ID ] ) || ( isset( $options['roles'] ) && count( array_intersect( $options['roles'], $user->roles ) ) > 0 ) )
				{
					if (strpos($actual_link, 'wp-admin') !== false)
					{
						wp_logout();
						header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
						header('Expires: Sat, 26 Jul 1997 05:00:00 GMT');
						auth_redirect();
						exit;
					}
				}
			}
		}
    }
}
endif;