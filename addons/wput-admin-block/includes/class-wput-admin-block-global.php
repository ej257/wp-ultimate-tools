<?php if ( ! defined( 'WPINC' ) ) { die( "Don't mess with us." ); }
/**
 * Hooks, actions and other helpers
 *
 * @since      1.0.0
 * @package    WP_Ultimate_Tools
 * @author     Goran Zivkovic
 */

if( !class_exists( 'WPUT_BLOCK_GLOBAL' ) ) :
class WPUT_BLOCK_GLOBAL extends WPUT_Global
{
    /**
     * All addon options
     */
    public $default_options_admin_block = array(
        'roles'     => array(),
        'users'     => array() 
    );

    /**
     * Class constructor
     */
    function __construct()
    {
        
    }
}
endif;