<?php if ( ! defined( 'WPINC' ) ) { die( "Don't mess with us." ); }
/**
 * Main Auto Login Addon Class
 *
 * @since      1.0.0
 * @package    WP_Ultimate_Tools
 * @author     Goran Zivkovic
 */

if( !class_exists( 'WPUT_BLOCK_INIT' ) ) :
class WPUT_BLOCK_INIT extends WPUT_BLOCK_GLOBAL
{
    /**
     * Run all plugin functions
     */
    public function run_blocker()
    {
        /**
         * Admin
         */
        if( file_exists( WPUT_BLOCK_INCLUDES . '/class-wput-admin-block-admin.php' ) )
        {
            require_once WPUT_BLOCK_INCLUDES . '/class-wput-admin-block-admin.php';
            if( class_exists( 'WPUT_BLOCK_ADMIN' ) ) new WPUT_BLOCK_ADMIN;
        }

        /**
         * Blocker
         */
        if( file_exists( WPUT_BLOCK_INCLUDES . '/class-wput-admin-block-blocker.php' ) )
        {
            require_once WPUT_BLOCK_INCLUDES . '/class-wput-admin-block-blocker.php';
            if( class_exists( 'WPUT_BLOCK_BLOCKER' ) ) new WPUT_BLOCK_BLOCKER;
        }
    }
}
endif;