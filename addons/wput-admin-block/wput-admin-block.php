<?php if ( ! defined( 'WPINC' ) ) { die( "Don't mess with us." ); }
/**
 * Addoin for admin blocking
 *
 * @since      1.0.0
 * @version    1.0.0
 * @package    WP_Ultimate_Tools
 * @author     Goran Zivkovic
 */

/**
 * Defines
 */
// Main plugin file
if ( ! defined( 'WPUT_BLOCK_FILE' ) )				define( 'WPUT_BLOCK_FILE', __FILE__ );
// Current plugin version
if ( ! defined( 'WPUT_BLOCK_VERSION' ) )			define( 'WPUT_BLOCK_VERSION', '1.0.0');
// Plugin root
if ( ! defined( 'WPUT_BLOCK_ROOT' ) )				define( 'WPUT_BLOCK_ROOT', rtrim(plugin_dir_path(WPUT_BLOCK_FILE), '/') );
// Includes directory
if ( ! defined( 'WPUT_BLOCK_INCLUDES' ) )			define( 'WPUT_BLOCK_INCLUDES', WPUT_BLOCK_ROOT . '/includes' );
// Includes directory
if ( ! defined( 'WPUT_BLOCK_ADMIN' ) )			    define( 'WPUT_BLOCK_ADMIN', WPUT_BLOCK_ROOT . '/admin' );
// Plugin URL root
if ( ! defined( 'WPUT_BLOCK_URL' ) )				define( 'WPUT_BLOCK_URL', rtrim(plugin_dir_url( WPUT_BLOCK_FILE ), '/') );
// Assets URL
if ( ! defined( 'WPUT_BLOCK_ASSETS' ) )            define( 'WPUT_BLOCK_ASSETS', WPUT_BLOCK_URL.'/assets' );
// Plugin name
if ( ! defined( 'WPUT_BLOCK_NAME' ) )				define( 'WPUT_BLOCK_NAME', 'wput-admin-block');
// Plugin session prefix (controlled by version)
if ( ! defined( 'WPUT_BLOCK_PREFIX' ) )		    define( 'WPUT_BLOCK_PREFIX', 'wput_admin_block_'.preg_replace("/[^0-9]/Ui",'',WPUT_BLOCK_VERSION).'_');
// Plugin prefix for hooks and others
if( ! defined( 'WPUT_BLOCK_HOOK' ) )               define( 'WPUT_BLOCK_HOOK', 'wput_admin_block_' );
// Plugin options name
if( ! defined( 'WPUT_BLOCK_OPTIONS' ) )            define( 'WPUT_BLOCK_OPTIONS', 'wput_admin_block_options' );

if( !class_exists( 'WPUT_BLOCK_GLOBAL' ) && !class_exists( 'WPUT_BLOCK_INIT' ) )
{
    /**
     * Include hook class
     */
    include_once WPUT_BLOCK_INCLUDES . '/class-wput-admin-block-global.php';

    if( class_exists( 'WPUT_BLOCK_GLOBAL' ) )
    {
        /**
         * Call hook class and make all options global
         */
        $hook = new WPUT_BLOCK_GLOBAL;

        $options = $hook->get_option( WPUT_BLOCK_OPTIONS ); 
        if( empty( $options ) )
        {
            $hook->update_option( WPUT_BLOCK_OPTIONS, $hook->default_options_admin_block );
            $options = $hook->get_option( WPUT_BLOCK_OPTIONS );
        }
        $GLOBALS['WPUT_ADMIN_BLOCK'] = $options;
        /**
         * Include main addon class
         */
        include_once WPUT_BLOCK_INCLUDES . '/class-wput-admin-block.php';

        if( class_exists( 'WPUT_BLOCK_INIT' ) )
        {
            class WPUT_BLOCK_LOAD extends WPUT_BLOCK_INIT
            {
                function __construct()
                {
                    //$this->add_action( WPUT_HOOK . 'activate', 'activate_auto_login' );
                    //$this->add_action( WPUT_HOOK . 'deactivate', 'deactivate_auto_login' );
                    do_action( WPUT_BLOCK_HOOK . 'init' );
                    $this->run_blocker();
                }
            }
        }
        /**
         * Clear CPU and RAM
         */
        $hook = NULL;
    }

    /**
     * Everything is probably loaded
     */
    function wput_admin_block()
    {
        if( class_exists( 'WPUT_BLOCK_LOAD' ) ) return new WPUT_BLOCK_LOAD;
    }

    /**
     * Let's start the show
     */
    wput_admin_block();
}