(function($){
    /**
     * Ajax for generating super-token
     */
    $( 'div#generate-token' ).on( 'click', function() {
        $.ajax({
            url: WPUT_LOGIN.ajax_url,
            cache: false,
            data: {
                action: 'wput_super_token'
            }
        }).done( function( d ) {
            $( '#super-token' ).val( d );
        }).fail( function( jqXHR, textStatus, errorThrown ) {
            console.log( jqXHR );
            //console.log( textStatus );
            console.log( errorThrown );
        });

        return false;
    });
})(jQuery || window.jQuery || Zepto || window.Zepto);