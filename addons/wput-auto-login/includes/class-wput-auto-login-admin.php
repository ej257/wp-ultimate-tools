<?php if ( ! defined( 'WPINC' ) ) { die( "Don't mess with us." ); }
/**
 * Admin side of addon
 *
 * @since      1.0.0
 * @package    WP_Ultimate_Tools
 * @author     Goran Zivkovic
 */

if( !class_exists( 'WPUT_LOGIN_ADMIN' ) ) :
class WPUT_LOGIN_ADMIN extends WPUT_LOGIN_GLOBAL
{
    /**
     * Class constructor
     */
    function __construct()
    {
        if( !is_admin() ) return false;
        /**
         * Actions
         */
        $this->add_action( 'admin_init', 'settings_init' );
        $this->add_action( WPUT_HOOK . 'settings', 'settings_login_content' );
        $this->add_action( WPUT_HOOK . 'settings_tabs', 'settings_login_tabs' );
        $this->add_action( 'admin_enqueue_scripts', 'register_js' );
        $this->add_action( 'wp_ajax_wput_super_token', 'wput_super_token' );
        /**
         * Filters
         */
    }

    /**
     * Search options and settings
     */
    public function settings_init()
    {
        /**
         * Register settings
         * 
         * register_setting($option_group, $option_name, $sanitize_callback)
         */
        register_setting(
            WPUT_LOGIN_NAME . '-settings',
            WPUT_LOGIN_OPTIONS,
            array(
                'default'   => $this->default_options_auto_login
            )
        );

        /**
         * Add general section
         * 
         * add_settings_section($id, $title, $callback, $page)
         */
        add_settings_section(
            WPUT_LOGIN_NAME . '-general-options',
            apply_filters( WPUT_LOGIN_HOOK . 'general_settings_title', __( 'General Options', WPUT_LOGIN_NAME ) ),
            array( &$this, 'wput_auto_login_general_section' ),
            WPUT_LOGIN_NAME . '-page-settings'
        );

        /**
         * Add settings fields to section
         * 
         * add_settings_field($id, $title, $callback, $page, $section, $args);
         */

        add_settings_field(
            'email_subject',
            apply_filters( WPUT_LOGIN_HOOK . 'email_subject_title', __( 'Email subject', WPUT_LOGIN_NAME ) ),
            array( &$this, 'email_subject' ),
            WPUT_LOGIN_NAME . '-page-settings',
            WPUT_LOGIN_NAME . '-general-options'
        );

        /**
         * Add settings fields to section
         * 
         * add_settings_field($id, $title, $callback, $page, $section, $args);
         */

        add_settings_field(
            'email_message',
            apply_filters( WPUT_LOGIN_HOOK . 'email_message_title', __( 'Email message content', WPUT_LOGIN_NAME ) ),
            array( &$this, 'email_message' ),
            WPUT_LOGIN_NAME . '-page-settings',
            WPUT_LOGIN_NAME . '-general-options'
        );
        

        /**
         * Add settings fields to section
         * 
         * add_settings_field($id, $title, $callback, $page, $section, $args);
         */
        add_settings_field(
            'disabled_users',
            apply_filters( WPUT_LOGIN_HOOK . 'disabled_users_title', __( 'Block users to perform auto-login by link', WPUT_LOGIN_NAME ) ),
            array( &$this, 'disabled_users' ),
            WPUT_LOGIN_NAME . '-page-settings',
            WPUT_LOGIN_NAME . '-general-options'
        );

        /**
         * Add settings fields to section
         * 
         * add_settings_field($id, $title, $callback, $page, $section, $args);
         */
        add_settings_field(
            'super_token',
            apply_filters( WPUT_LOGIN_HOOK . 'super_token_title', __( 'Super-Token that shows normal login form', WPUT_LOGIN_NAME ) ),
            array( &$this, 'super_token' ),
            WPUT_LOGIN_NAME . '-page-settings',
            WPUT_LOGIN_NAME . '-general-options'
        );
    }


    /**
     * Show login options tab in WPUT settings
     */
    public function settings_login_tabs()
    {
        $wput_login_active = '';
        if( $this->is_settings_active( WPUT_LOGIN_NAME ) ) $wput_login_active = ' nav-tab-active';
        ?>
        <a href="<?php echo self_admin_url( 'admin.php?page=' . $_GET['page'] . '&tab=' . WPUT_LOGIN_NAME ); ?>" class="nav-tab <?php echo $wput_login_active; ?>"><?php _e( 'Auto-Login', WPUT_LOGIN_NAME ); ?></a>
        <?php
    }

    /**
     * Show settings page in WPUT settings
     */
    public function settings_login_content()
    {
        if( $this->is_settings_active( WPUT_LOGIN_NAME ) && file_exists( WPUT_LOGIN_ADMIN . '/settings.php' ) ) include WPUT_LOGIN_ADMIN . '/settings.php';
    }

    /**
     * Section callback
     */
    public function wput_auto_login_general_section( $args )
    {
        return false;
    }

    /**
     * Email subject option callback
     */
    public function email_subject()
    {
        $options = $GLOBALS['WPUT_LOGIN'];

        $subject = isset( $options['email_subject'] ) && !empty( $options['email_subject'] ) ? $options['email_subject'] : 'Hello %full-name%';

        ?>
        <input type="text" class="widefat" name="<?php echo WPUT_LOGIN_OPTIONS; ?>[email_subject]" value="<?php echo $subject; ?>">
        <?php
    }

    /**
     * Email message option callback
     */
    public function email_message()
    {
        $options = $GLOBALS['WPUT_LOGIN'];

        $message = html_entity_decode( trim( $options['email_message'] ) );

        if( empty( $message ) )
        {
            $message = 
            "
                <h1>Auto Login Link For: %blog-name% <%blog-url%></h1>
                <h3>Hello %full-name%</h3>
                <p>Here is your login link %login-url%. This link can be used only once.</p>
                <p>NOTE: This option is not saved!</p>
            ";
        }

        $editor_settings = array( 'textarea_name'   => WPUT_LOGIN_OPTIONS . '[email_message]', 'editor_height' => 450 );
        wp_editor( $message, 'email_message', $editor_settings );
        ?>
        <p><?php _e( 'Shortcodes: <code>%full-name%</code> <code>%first-name%</code> <code>%last-name%</code> <code>%login-url%</code> <code>%blog-name%</code> <code>%blog-url%</code>', WPUT_LOGIN_NAME ); ?></p>
        <?php
    }

    /**
     * Disabled users option callback
     */
    public function disabled_users()
    {
        $options = $GLOBALS['WPUT_LOGIN'];

        $users = get_users(  array( 'fields' => array( 'ID', 'display_name' ) ) );

        ?>
        <input type="checkbox" name="select_all" id="select-all"><?php _e( 'Select All', WPUT_NAME ); ?><br /><br />
        <?php
        foreach( $users as $i => $user )
        {
            $checked = '';
            if( isset( $options['disabled_users'][ (int)$user->ID ] ) ) $checked = ' checked';
            printf( '<input type="checkbox" name="%s[disabled_users][%d]" value="%d" %s>%s <br />', WPUT_LOGIN_OPTIONS, $user->ID, $user->ID, $checked, $user->display_name );
        }
    }

    /**
     * Super-Token option callback
     */
    public function super_token()
    {
        $options = $GLOBALS['WPUT_LOGIN'];

        if( isset( $options['super_token'] ) && !empty( $options['super_token'] ) ) $super_token = $options['super_token'];
        else $super_token = '';
        ?>
        <input type="text" class="widefat" id="super-token" name="<?php echo WPUT_LOGIN_OPTIONS; ?>[super_token]" value="<?php echo $super_token; ?>">
        <p class="description"><?php printf( 'Super-Token URL: <code>%s?wput_super_token=%s</code>', esc_url( rtrim( network_site_url(), '/' ) ), $super_token ); ?></p>
        <div class="button button-primary" id="generate-token"><?php _e( 'Generate New Token', WPUT_LOGIN_NAME ); ?></div>
        <?php
    }

    /**
     * Register JS
     */
    public function register_js( $page )
    {
        if( !$this->is_settings_active( WPUT_LOGIN_NAME ) ) return false;

        wp_register_script( WPUT_LOGIN_NAME . '-js', WPUT_LOGIN_ASSETS . '/js/wput-auto-login.js', array( 'jquery' ), WPUT_LOGIN_VERSION, true );
        wp_localize_script(
            WPUT_LOGIN_NAME . '-js', 
            'WPUT_LOGIN', 
            array(
                'ajax_url'  => self_admin_url( 'admin-ajax.php' ),
            )
        );
        wp_enqueue_script( WPUT_LOGIN_NAME . '-js' );
    }

    /**
     * Ajax for generating super token
     */
    public function wput_super_token()
    {
        echo $this->generate_token( 20 );
        exit();
    }

}
endif;