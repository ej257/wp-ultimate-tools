<?php if ( ! defined( 'WPINC' ) ) { die( "Don't mess with us." ); }
/**
 * Hooks, actions and other helpers
 *
 * @since      1.0.0
 * @package    WP_Ultimate_Tools
 * @author     Goran Zivkovic
 */

if( !class_exists( 'WPUT_LOGIN_GLOBAL' ) ) :
class WPUT_LOGIN_GLOBAL extends WPUT_Global
{
    /**
     * All addon options
     */
    public $default_options_auto_login = array(
        'email_subject'     => '',
        'email_message'     => '',
		'disabled_users'    => array(),
		'super_token'		=> ''
    );

    /**
     * Class constructor
     */
    function __construct()
    {
        
    }

    /*
	* Generate token
	*/
	public function generate_token($length=16){
		if(function_exists('openssl_random_pseudo_bytes') || function_exists('random_bytes'))
		{
			if (version_compare(PHP_VERSION, '7.0.0') >= 0)
				return str_rot13(bin2hex(random_bytes($length)));
			else
				return str_rot13(bin2hex(openssl_random_pseudo_bytes($length)));
		}
		else
		{
			return md5(str_replace(array('.',' ','_'),mt_rand(1000,9999),uniqid('t'.microtime())));
		}
	}

	/**
	 * Generate Auto-Login Url
	 */
	public function generate_login_url( $user_id = 0, $token )
	{
        if( empty( $user_id ) || (int)$user_id <= 0 ) return false;
        
        $user = get_user_by( 'id', (int)$user_id );
		if( $user === false ) return false;

		$user_id = absint( $user_id );

		update_user_meta( $user_id, 'token', $token );

		return sprintf( '<a href="%s?wput_token=%s&wput_id=%d" target="_blank">%s?wput_token=%s&wput_id=%d</a>', esc_url( rtrim( network_site_url(), '/' ) ), $token, $user_id, esc_url( rtrim( network_site_url(), '/' ) ), $token, $user_id );
	}

	/**
	 * Convert shortcode to value
	 */
	public function shortcode_to_value( $str, $user, $token )
	{
		return str_replace(array(
			'%full-name%',
			'%first-name%',
			'%last-name%',
			'%login-url%',
			'%blog-name%',
			'%blog-url%',
		),
		array(
			$user->display_name,
			$user->user_firstname,
			$user->user_lastname,
			$this->generate_login_url( $user->ID, $token ),
			get_bloginfo( 'name' ),
			get_bloginfo( 'url' ),
		), 
		$str);
	}

	/**
	 * Check Super-Token
	 */
	public function check_super_token()
	{
		$options = $GLOBALS['WPUT_LOGIN'];
		if( isset( $_GET['redirect_to'] ) && strpos( $_GET['redirect_to'], 'wput_super_token' ) !== false )
        {
            $redirect_url = urldecode( $_GET['redirect_to'] );
            $wput_super_token = '';

            $i = strpos( $redirect_url, '=' );
			if( $i !== false ) $wput_super_token = substr( $redirect_url, $i + 1, strlen( $redirect_url ) );

			if( isset( $options['super_token'] ) && $options['super_token'] === $wput_super_token ) return true;
			else return false;
		}
		
		return '-1';
	}
}
endif;