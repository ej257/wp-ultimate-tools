<?php if ( ! defined( 'WPINC' ) ) { die( "Don't mess with us." ); }
/**
 * Auto Login System
 *
 * @since      1.0.0
 * @package    WP_Ultimate_Tools
 * @author     Goran Zivkovic
 */

if( !class_exists( 'WPUT_LOGIN_SYSTEM' ) ) :
class WPUT_LOGIN_SYSTEM extends WPUT_LOGIN_GLOBAL
{
    /**
     * Class constructor
     */
    function __construct()
    {
        /**
         * Actions
         */
        $this->add_action( 'init', 'wput_auto_login_auth' );

        if( is_admin() ) return false;

        $this->add_action( 'login_enqueue_scripts', 'wput_login_style' );

        /**
         * Filters
         */
        $this->add_filter( 'wp_login_errors', 'wput_login_form', 90, 2 );
        $this->add_filter( 'login_message', 'wput_login_authenticate' );
    }

    /**
     * Auto Login
     */
    public function wput_auto_login_auth()
    {
        if( !is_user_logged_in() )
        {
            if( isset( $_GET['wput_token'] ) && isset( $_GET['wput_id'] ) )
            {
                $wput_token = $_GET['wput_token'];
                $wput_user_id = absint( $_GET['wput_id'] );

                $user = get_user_by( 'id', $wput_user_id );

                if( $user !== false ) 
                {
                    $token = get_user_meta( $wput_user_id, 'token', true );
                    if( $token == $wput_token )
                    {
                        wp_clear_auth_cookie();
                        $secure_cookie = '';
                        if( get_user_option( 'use_ssl', $user->ID ) )
                        {
                            $secure_cookie = true;
                            force_ssl_admin( true );
                        }
                
                        wp_set_current_user( $user->ID, $user->user_login );
                        wp_set_auth_cookie( $user->ID );
                        do_action( 'wp_login', $user->user_login, $user );

                        delete_user_meta( $user->ID, 'token' );
                        if( is_user_logged_in() )
                        {
                            /**
                             * WP style redirect
                             */
                            $redirect_to = home_url( 'wp-admin/' );
                            if ( is_multisite() && !get_active_blog_for_user( $user->ID ) && !is_super_admin( $user->ID ) ) $redirect_to = user_admin_url();
                            elseif ( is_multisite() && !$user->has_cap( 'read' ) ) $redirect_to = get_dashboard_url( $user->ID );
                            elseif ( !$user->has_cap( 'edit_posts' ) ) $redirect_to = $user->has_cap( 'read' ) ? self_admin_url( 'profile.php' ) : home_url();
                            
                            wp_redirect( $redirect_to );
                            exit();
                        }
                        else
                        {
                            auth_redirect();
                            exit();
                        }
                    }
                    else
                    {
                        auth_redirect();
                        exit();
                    }
                }
                else
                {
                    auth_redirect();
                    exit();
                }
            }
            if( isset( $_GET['wput_super_token'] ) ) 
            {
                auth_redirect();
                exit();
            }
        }
    }

    /**
     * WPUT Auto-Login Style
     */
    public function wput_login_style()
    {
        wp_enqueue_style( WPUT_LOGIN_NAME . '-css', WPUT_LOGIN_ASSETS . '/css/wput-auto-login.css', array(), WPUT_LOGIN_VERSION, 'all' );
    }

    /**
     * Create auto-login form
     */
    public function wput_login_form( $errors, $redirect_to )
    {
        $wput_super_token = $this->check_super_token();
        if( $wput_super_token === false || $wput_super_token === '-1' )
        {
            login_header( __('Log In'), '', $errors);
            echo "</div>";
            ?>
            <form action="<?php echo esc_url( site_url( 'wp-login.php', 'login' ) ); ?>" id="wput-auto-login" method="post">
                <p>
                    <label for="wput-login-email">
                        <strong><?php _e( 'User Email Address', WPUT_LOGIN_NAME ); ?></strong>
                        <input type="text" id="wput-login-email" class="input" size="20" name="wput_login_email">
                    </label>
                </p>
                <p class="forgetmenot">
                    <p class="description"><?php _e( 'Same email will be used for reciving email with login link.', WPUT_LOGIN_NAME ); ?></p>
                </p>
                <input type="submit" name="wput_login_submit" class="button button-primary button-large" value="Generate Link">
            </form>
            <?php
            do_action( 'login_footer' );
            echo "</body></html>";
            exit;
        }
        return $errors;
    }

    /**
     * Authenticate
     */
    public function wput_login_authenticate( $message )
    {
        if( isset( $_POST['wput_login_submit'] ) && !isset( $_POST['wp-submit'] ) )
        {
            $options = $GLOBALS['WPUT_LOGIN'];
           
            $email = $_POST['wput_login_email'];

            $user = get_user_by( 'email', $email );

            if( $user === false ) $message = sprintf( '<p class="message"><b>%s</b> %s</p>', __( 'ERROR:', WPUT_LOGIN_NAME ), __( 'User with inserted email does not exists.', WPUT_LOGIN_NAME ) );
            elseif( isset( $options['disabled_users'][ (int)$user->ID ] ) ) 
            {
                $message = sprintf( '<p class="message"><b>%s</b> %s</p>', __( 'ERROR:', WPUT_LOGIN_NAME ), __( 'For this user auto-login is forbidden by admin.', WPUT_LOGIN_NAME ) );
                delete_user_meta( $user->ID, 'token' );
            }
            else
            {
                $token = $this->generate_token();
                $email_message = $this->shortcode_to_value( $options['email_message'], $user, $token );
                $email_subject = $this->shortcode_to_value( $options['email_subject'], $user, $token );
                
                $email = wp_mail( $user->user_email, $email_subject, $email_message, array('Content-Type: text/html; charset=UTF-8') );
                if( $email ) $message = sprintf( '<p class="message"><b>%s</b> %s</p>', __( 'SUCCESS:', WPUT_LOGIN_NAME ), __( 'Link for auto-login has been sent.', WPUT_LOGIN_NAME ) );
                else 
                {
                    $message = sprintf( '<p class="message"><b>%s</b> %s</p>', __( 'ERROR:', WPUT_LOGIN_NAME ), __( 'Server is currently unable to send email. Please try again later.', WPUT_LOGIN_NAME ) );
                    delete_user_meta( $user->ID, 'token' );
                }
                var_dump( get_user_meta( $user->ID, 'token' ) );
            }
        }

        if( isset( $_GET['redirect_to'] ) && strpos( $_GET['redirect_to'], 'wput_token' ) !== false )
        {
            $message = sprintf( 
                '<p class="message"><b>%s</b> %s<br />
                    %s<br />
                    %s<br />
                    %s<br />
                </p>', __( 'ERROR:', WPUT_LOGIN_NAME ), __( 'Auto-Login Failed.<br />Possible reasons:', WPUT_LOGIN_NAME ), __( '1. Token is invalid or is already used', WPUT_LOGIN_NAME ), __('2. User created new token for login', WPUT_LOGIN_NAME ), __( '3. In the meantime user account is deleted', WPUT_LOGIN_NAME )
            );
        }

        if( !$this->check_super_token() ) $message = sprintf( '<p class="message"><b>%s</b> %s', __( 'ERROR:', WPUT_LOGIN_NAME ), __( 'Invalid Super-Token.', WPUT_LOGIN_NAME ) );

        return $message;
    }
}
endif;