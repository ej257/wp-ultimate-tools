<?php if ( ! defined( 'WPINC' ) ) { die( "Don't mess with us." ); }
/**
 * Main Auto Login Addon Class
 *
 * @since      1.0.0
 * @package    WP_Ultimate_Tools
 * @author     Goran Zivkovic
 */

if( !class_exists( 'WPUT_LOGIN_INIT' ) ) :
class WPUT_LOGIN_INIT extends WPUT_LOGIN_GLOBAL
{
    /**
     * Run all plugin functions
     */
    public function run_login()
    {
        /**
         * Auto-Login System
         */
        if( file_exists( WPUT_LOGIN_INCLUDES . '/class-wput-auto-login-sys.php' ) )
        {
            require_once WPUT_LOGIN_INCLUDES . '/class-wput-auto-login-sys.php';
            if( class_exists( 'WPUT_LOGIN_SYSTEM' ) ) new WPUT_LOGIN_SYSTEM;
        }

        /**
         * Admin
         */
        if( file_exists( WPUT_LOGIN_INCLUDES . '/class-wput-auto-login-admin.php' ) )
        {
            require_once WPUT_LOGIN_INCLUDES . '/class-wput-auto-login-admin.php';
            if( class_exists( 'WPUT_LOGIN_ADMIN' ) ) new WPUT_LOGIN_ADMIN;
        }
    }
}
endif;