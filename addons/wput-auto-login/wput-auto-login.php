<?php if ( ! defined( 'WPINC' ) ) { die( "Don't mess with us." ); }
/**
 * Addoin for auto-login
 *
 * @since      1.0.0
 * @version    1.0.0
 * @package    WP_Ultimate_Tools
 * @author     Goran Zivkovic
 */

/**
 * Defines
 */
// Main plugin file
if ( ! defined( 'WPUT_LOGIN_FILE' ) )				define( 'WPUT_LOGIN_FILE', __FILE__ );
// Current plugin version
if ( ! defined( 'WPUT_LOGIN_VERSION' ) )			define( 'WPUT_LOGIN_VERSION', '1.0.0');
// Plugin root
if ( ! defined( 'WPUT_LOGIN_ROOT' ) )				define( 'WPUT_LOGIN_ROOT', rtrim(plugin_dir_path(WPUT_LOGIN_FILE), '/') );
// Includes directory
if ( ! defined( 'WPUT_LOGIN_INCLUDES' ) )			define( 'WPUT_LOGIN_INCLUDES', WPUT_LOGIN_ROOT . '/includes' );
// Includes directory
if ( ! defined( 'WPUT_LOGIN_ADMIN' ) )			    define( 'WPUT_LOGIN_ADMIN', WPUT_LOGIN_ROOT . '/admin' );
// Plugin URL root
if ( ! defined( 'WPUT_LOGIN_URL' ) )				define( 'WPUT_LOGIN_URL', rtrim(plugin_dir_url( WPUT_LOGIN_FILE ), '/') );
// Assets URL
if ( ! defined( 'WPUT_LOGIN_ASSETS' ) )            define( 'WPUT_LOGIN_ASSETS', WPUT_LOGIN_URL.'/assets' );
// Plugin name
if ( ! defined( 'WPUT_LOGIN_NAME' ) )				define( 'WPUT_LOGIN_NAME', 'wput-auto-login');
// Plugin session prefix (controlled by version)
if ( ! defined( 'WPUT_LOGIN_PREFIX' ) )		    define( 'WPUT_LOGIN_PREFIX', 'wput_auto_login_'.preg_replace("/[^0-9]/Ui",'',WPUT_LOGIN_VERSION).'_');
// Plugin prefix for hooks and others
if( ! defined( 'WPUT_LOGIN_HOOK' ) )               define( 'WPUT_LOGIN_HOOK', 'wput_auto_login_' );
// Plugin options name
if( ! defined( 'WPUT_LOGIN_OPTIONS' ) )            define( 'WPUT_LOGIN_OPTIONS', 'wput_auto_login_options' );

if( !class_exists( 'WPUT_LOGIN_GLOBAL' ) && !class_exists( 'WPUT_LOGIN_INIT' ) )
{
    /**
     * Include hook class
     */
    include_once WPUT_LOGIN_INCLUDES . '/class-wput-auto-login-global.php';

    if( class_exists( 'WPUT_LOGIN_GLOBAL' ) )
    {
        /**
         * Call hook class and make all options global
         */
        $hook = new WPUT_LOGIN_GLOBAL;

        $options = $hook->get_option( WPUT_LOGIN_OPTIONS ); 
        if( empty( $options ) )
        {
            $hook->update_option( WPUT_LOGIN_OPTIONS, $hook->default_options_auto_login );
            $options = $hook->get_option( WPUT_LOGIN_OPTIONS );
        }
        $GLOBALS['WPUT_LOGIN'] = $options;
        /**
         * Include main addon class
         */
        include_once WPUT_LOGIN_INCLUDES . '/class-wput-auto-login.php';

        if( class_exists( 'WPUT_LOGIN_INIT' ) )
        {
            class WPUT_LOGIN_LOAD extends WPUT_LOGIN_INIT
            {
                function __construct()
                {
                    //$this->add_action( WPUT_HOOK . 'activate', 'activate_auto_login' );
                    //$this->add_action( WPUT_HOOK . 'deactivate', 'deactivate_auto_login' );
                    do_action( WPUT_LOGIN_HOOK . 'init' );
                    $this->run_login();
                }
            }
        }
        /**
         * Clear CPU and RAM
         */
        $hook = NULL;
    }

    /**
     * Everything is probably loaded
     */
    function wput_auto_login()
    {
        if( class_exists( 'WPUT_LOGIN_LOAD' ) ) return new WPUT_LOGIN_LOAD;
    }

    /**
     * Let's start the show
     */
    wput_auto_login();
}