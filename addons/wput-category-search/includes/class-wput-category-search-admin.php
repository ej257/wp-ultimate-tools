<?php if ( ! defined( 'WPINC' ) ) { die( "Don't mess with us." ); }
/**
 * Admin side of addon
 *
 * @since      1.0.0
 * @package    WP_Ultimate_Tools
 * @author     Goran Zivkovic
 */
if( !class_exists( 'WPUT_SEARCH_ADMIN' ) ) :
class WPUT_SEARCH_ADMIN extends WPUT_SEARCH_GLOBAL
{
    /**
     * Class constructor
     */
    function __construct()
    {
        /**
         * Actions
         */
        $this->add_action( 'init', 'load_scripts' );
        $this->add_action( 'admin_init', 'settings_init' );
        $this->add_action( WPUT_HOOK . 'settings', 'settings_search_content' );
        $this->add_action( WPUT_HOOK . 'settings_tabs', 'settings_search_tabs' );

        /**
         * Filters
         */
    }

    /**
     * Load all addon scripts
     */
    public function load_scripts()
    {
        /**
         * Load JS for search
         */
        $this->add_action( 'admin_enqueue_scripts', 'register_javascript' );
    }

    /**
     * JS for category search
     */
    public function register_javascript( $page )
    {
        $options = $GLOBALS['WPUT_CATEGORY_SEARCH_OPTIONS'];
        $allowed_screens = array(
            'post.php',
            'post-new.php'
        );

        if( !in_array( $page, $allowed_screens ) ) return false;

        if( isset( $_GET['post_type'] ) )
        {
            if( $_GET['post_type'] === 'cf-geoplugin-banner' || !isset( $options['post_types'][ $_GET['post_type'] ] ) ) return false;
        }
        elseif( !isset( $options['post_types']['post'] ) ) return false;

        wp_register_script( WPUT_SEARCH_NAME . '-js', WPUT_SEARCH_ASSETS . '/js/wput-category-search.js', array( 'jquery' ), WPUT_SEARCH_VERSION, true );
        wp_enqueue_script( WPUT_SEARCH_NAME . '-js' );
        wp_localize_script(
            WPUT_SEARCH_NAME . '-js',
            'WPUT_SEARCH',
            array(
                'ajaxurl' => self_admin_url( 'admin-ajax.php' ),
			    'label' => array(
				    'loading' => __( 'Loading...', WPUT_SEARCH_NAME ),
				    'not_found' => __( 'Not Found!', WPUT_SEARCH_NAME ),
				    'placeholder' => __( 'Search', WPUT_SEARCH_NAME )
			    )
            )
        );
    } 

    /**
     * Search options and settings
     */
    public function settings_init()
    {
        /**
         * Register settings
         * 
         * register_setting($option_group, $option_name, $sanitize_callback)
         */
        register_setting(
            WPUT_SEARCH_NAME . '-settings',
            WPUT_SEARCH_OPTIONS,
            array(
                'default'   => $this->default_options_category_search
            )
        );

        /**
         * Add general section
         * 
         * add_settings_section($id, $title, $callback, $page)
         */
        add_settings_section(
            WPUT_SEARCH_NAME . '-general-options',
            apply_filters( WPUT_SEARCH_HOOK . 'general_settings_title', __( 'General Options', WPUT_SEARCH_NAME ) ),
            array( &$this, 'wput_category_search_general_section' ),
            WPUT_SEARCH_NAME . '-page-settings'
        );

        /**
         * Add settings fields to section
         * 
         * add_settings_field($id, $title, $callback, $page, $section, $args);
         */
        add_settings_field(
            'post_types',
            apply_filters( WPUT_SEARCH_HOOK . 'post_types_title', __( 'Show category search in', WPUT_SEARCH_NAME ) ),
            array( &$this, 'post_types' ),
            WPUT_SEARCH_NAME . '-page-settings',
            WPUT_SEARCH_NAME . '-general-options'
        );
    }

    /**
     * Section callback
     */
    public function wput_category_search_general_section( $args )
    {
        return false;
    }

    /**
     * Post types option callback
     */
    public function post_types()
    {
        $options = $GLOBALS['WPUT_CATEGORY_SEARCH_OPTIONS'];

        $all_post_types = get_post_types(
            array(
                'public'    => true,
                '_builtin'   => false
            ),
            'objects'
        );
        ?>
        <input type="checkbox" name="select_all" id="select-all"><?php _e( 'Select All', WPUT_NAME ); ?><br /><br />
        <input type="checkbox" name="<?php echo WPUT_SEARCH_OPTIONS ?>[post_types][post]" value="post" <?php if( isset( $options['post_types']['post'] ) ) echo ' checked'; ?>>Post [post]<br />
        <?php
        if( !empty( $all_post_types ) )
        {
            foreach( $all_post_types as $post_type )
            {
                if( !isset( $post_type->name ) ) continue;
                if( !isset( $post_type->label ) || $post_type->name === 'cf-geoplugin-banner' ) $label = 'Unknown';
                else $label = $post_type->label;
                $checked = '';
                if( isset( $options['post_types'][$post_type->name] ) ) $checked = ' checked';
                ?>                    
                <input type="checkbox" name="<?php echo WPUT_SEARCH_OPTIONS; ?>[post_types][<?php echo $post_type->name; ?>]" value="<?php echo $post_type->name; ?>" <?php echo $checked; ?>><?php echo $label; ?> [<?php echo $post_type->name; ?>]<br />
                <?php
            }
        }
        ?>
        <p class="description"><?php _e( 'Select where to show category search', WPUT_SEARCH_NAME ); ?></p>

        <?php
    }

    /**
     * Show settings page in WPUT settings
     */
    public function settings_search_content()
    {
        if( $this->is_settings_active( WPUT_SEARCH_NAME ) && file_exists( WPUT_SEARCH_ADMIN . '/settings.php' ) ) include WPUT_SEARCH_ADMIN . '/settings.php';
    }

    /**
     * Show search options tab in WPUT settings
     */
    public function settings_search_tabs()
    {
        $wput_search_active = '';
        if( $this->is_settings_active( WPUT_SEARCH_NAME ) ) $wput_search_active = ' nav-tab-active';
        ?>
        <a href="<?php echo self_admin_url( 'admin.php?page=' . $_GET['page'] . '&tab=' . WPUT_SEARCH_NAME ); ?>" class="nav-tab <?php echo $wput_search_active; ?>"><?php _e( 'Category Search', WPUT_SEARCH_NAME ); ?></a>
        <?php
    }
}
endif;