<?php if ( ! defined( 'WPINC' ) ) { die( "Don't mess with us." ); }
/**
 * Hooks, actions and other helpers
 *
 * @since      1.0.0
 * @package    WP_Ultimate_Tools
 * @author     Goran Zivkovic
 */
if( !class_exists( 'WPUT_SEARCH_GLOBAL' ) ) :
class WPUT_SEARCH_GLOBAL extends WPUT_Global
{
    /**
     * All options
     */
    public $default_options_category_search = array(
        'post_types'      => array()
    );
}
endif;