<?php if ( ! defined( 'WPINC' ) ) { die( "Don't mess with us." ); }
/**
 * Main Category Search Addon Class
 *
 * @since      1.0.0
 * @package    WP_Ultimate_Tools
 * @author     Goran Zivkovic
 */
if( !class_exists( 'WPUT_SEARCH_INIT' ) ) :
class WPUT_SEARCH_INIT extends WPUT_SEARCH_GLOBAL
{
    /**
     * Run all addon functions
     */
    public function run_search()
    {
        /**
         * Admin
         */
        if( file_exists( WPUT_SEARCH_INCLUDES . '/class-wput-category-search-admin.php' ) )
        {
            require_once WPUT_SEARCH_INCLUDES . '/class-wput-category-search-admin.php';
            if( class_exists( 'WPUT_SEARCH_ADMIN' ) ) new WPUT_SEARCH_ADMIN;
        }
    }

    /**
     * Load addon text domain for translations
     */
    public function load_textdomain()
    {
        
    }
}
endif;