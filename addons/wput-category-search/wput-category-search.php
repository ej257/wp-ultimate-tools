<?php if ( ! defined( 'WPINC' ) ) { die( "Don't mess with us." ); }
/**
 * Addoin for category and post search
 *
 * @since      1.0.0
 * @version    1.0.0
 * @package    WP_Ultimate_Tools
 * @author     Goran Zivkovic
 */

/**
 * Defines
 */
// Main plugin file
if ( ! defined( 'WPUT_SEARCH_FILE' ) )				define( 'WPUT_SEARCH_FILE', __FILE__ );
// Current plugin version
if ( ! defined( 'WPUT_SEARCH_VERSION' ) )			define( 'WPUT_SEARCH_VERSION', '1.0.0');
// Plugin root
if ( ! defined( 'WPUT_SEARCH_ROOT' ) )				define( 'WPUT_SEARCH_ROOT', rtrim(plugin_dir_path(WPUT_SEARCH_FILE), '/') );
// Includes directory
if ( ! defined( 'WPUT_SEARCH_INCLUDES' ) )			define( 'WPUT_SEARCH_INCLUDES', WPUT_SEARCH_ROOT . '/includes' );
// Includes directory
if ( ! defined( 'WPUT_SEARCH_ADMIN' ) )			    define( 'WPUT_SEARCH_ADMIN', WPUT_SEARCH_ROOT . '/admin' );
// Plugin URL root
if ( ! defined( 'WPUT_SEARCH_URL' ) )				define( 'WPUT_SEARCH_URL', rtrim(plugin_dir_url( WPUT_SEARCH_FILE ), '/') );
// Assets URL
if ( ! defined( 'WPUT_SEARCH_ASSETS' ) )            define( 'WPUT_SEARCH_ASSETS', WPUT_SEARCH_URL.'/assets' );
// Plugin name
if ( ! defined( 'WPUT_SEARCH_NAME' ) )				define( 'WPUT_SEARCH_NAME', 'wput-category-search');
// Plugin session prefix (controlled by version)
if ( ! defined( 'WPUT_SEARCH_PREFIX' ) )		    define( 'WPUT_SEARCH_PREFIX', 'wput_category_search_'.preg_replace("/[^0-9]/Ui",'',WPUT_SEARCH_VERSION).'_');
// Plugin prefix for hooks and others
if( ! defined( 'WPUT_SEARCH_HOOK' ) )               define( 'WPUT_SEARCH_HOOK', 'wput_category_search_' );
// Plugin options name
if( ! defined( 'WPUT_SEARCH_OPTIONS' ) )            define( 'WPUT_SEARCH_OPTIONS', 'wput_category_search_options' );

if( !class_exists( 'WPUT_SEARCH_GLOBAL' ) && !class_exists( 'WPUT_SEARCH_INIT' ) )
{
    /**
     * Include hook class
     */
    include_once WPUT_SEARCH_INCLUDES . '/class-wput-category-search-global.php';

    if( class_exists( 'WPUT_SEARCH_GLOBAL' ) )
    {
        /**
         * Call hook class and make all options global
         */
        $hook = new WPUT_SEARCH_GLOBAL;

        $options = $hook->get_option( WPUT_SEARCH_OPTIONS ); 
        if( empty( $options ) )
        {
            $hook->update_option( WPUT_SEARCH_OPTIONS, $hook->default_options_category_search );
            $options = $hook->get_option( WPUT_SEARCH_OPTIONS );
        }
        $GLOBALS['WPUT_CATEGORY_SEARCH_OPTIONS'] = $options;
        /**
         * Include main addon class
         */
        include_once WPUT_SEARCH_INCLUDES . '/class-wput-category-search.php';

        if( class_exists( 'WPUT_SEARCH_INIT' ) )
        {
            class WPUT_SEARCH_LOAD extends WPUT_SEARCH_INIT
            {
                function __construct()
                {
                    //$this->add_action( WPUT_HOOK . 'activate', 'activate_duplicator' );
                    //$this->add_action( WPUT_HOOK . 'deactivate', 'deactivate_duplicator' );
                    do_action( WPUT_SEARCH_HOOK . 'init' );
                    $this->run_search();
                }
            }
        }
        /**
         * Clear CPU and RAM
         */
        $hook = NULL;
    }

    /**
     * Everything is probably loaded
     */
    function wput_category_search()
    {
        if( class_exists( 'WPUT_SEARCH_LOAD' ) ) return new WPUT_SEARCH_LOAD;
    }

    /**
     * Let's start the show
     */
    wput_category_search();
}