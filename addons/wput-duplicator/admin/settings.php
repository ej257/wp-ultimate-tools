<?php if ( ! defined( 'WPINC' ) ) { die( "Don't mess with us." ); }
/**
 * Settings Page
 *
 * @since      1.0.0
 * @package    WP_Ultimate_Tools
 * @author     Goran Zivkovic
 */
?>
<div id="post-body" class="metabox-holder columns-1">
    <div id="postbox-container" class="postbox-container-1">
        <form method="post" action="options.php">
            <div class="meta-box-sortables ui-sortable" id="normal-sortables">
                <div class="postbox " id="itsec_get_started">
                    <h3 class="hndle"><span><?php _e( 'WP Duplicator General Settings', WPUT_D_NAME ); ?></span></h3>
                    <div class="inside">
                        <?php 
                            settings_fields( WPUT_D_NAME . '-settings' );

                            do_settings_sections( WPUT_D_NAME . '-page-settings' );

                            submit_button();
                        ?>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>