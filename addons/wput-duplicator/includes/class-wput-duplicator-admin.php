<?php if ( ! defined( 'WPINC' ) ) { die( "Don't mess with us." ); }
/**
 * Admin side of addon
 *
 * @since      1.0.0
 * @package    WP_Ultimate_Tools
 * @author     Goran Zivkovic
 */
if( !class_exists( 'WPUT_D_ADMIN' ) ) :
class WPUT_D_ADMIN extends WPUT_D_GLOBAL
{
    /**
     * Class constructor
     */
    function __construct()
    {
        /**
         * Actions
         */
        $this->add_action( 'admin_init', 'settings_init' );
        $this->add_action( WPUT_HOOK . 'settings', 'settings_duplicator_content' );
        $this->add_action( WPUT_HOOK . 'settings_tabs', 'settings_duplicator_tabs' );

        /**
         * Filters
         */
    }

    /**
     * Duplicator options and settings
     */
    public function settings_init()
    {
        /**
         * Register options_group and options
         */
        register_setting( 
            WPUT_D_NAME . '-settings', 
            WPUT_D_OPTIONS,
            array(
                'default'   => $this->default_options_duplicator
            )
        );

        /**
         * Add new settings section
         * 
         * add_settings_section($id, $title, $callback, $page)
         */
        add_settings_section(
            WPUT_D_NAME . '-general-options',
            apply_filters( WPUT_D_HOOK . 'general_settings_title', __( 'General Options', WPUT_D_NAME ) ),
            array( &$this, 'wput_duplicator_general_section' ),
            WPUT_D_NAME . '-page-settings'
        );

        /**
         * Add settings fields to section
         * 
         * add_settings_field($id, $title, $callback, $page, $section, $args);
         */
        add_settings_field(
            'post_types',
            apply_filters( WPUT_D_HOOK . 'post_types_title', __( 'Show duplicator options in', WPUT_D_NAME ) ),
            array( &$this, 'post_types' ),
            WPUT_D_NAME . '-page-settings',
            WPUT_D_NAME . '-general-options'
        );

        /**
         * Add settings fields to section
         * 
         * add_settings_field($id, $title, $callback, $page, $section, $args);
         */
        add_settings_field(
            'delete',
            apply_filters( WPUT_D_HOOK . 'delete_title', __( 'Show "Delete Permanenlty" option in', WPUT_D_NAME ) ),
            array( &$this, 'delete' ),
            WPUT_D_NAME . '-page-settings',
            WPUT_D_NAME . '-general-options'
        );
    }

    /**
     * General Options Section Callback
     */
    public function wput_duplicator_general_section( $args )
    {
        return false;
    }

    /**
     * Show all public post types to user to choose
     */
    public function post_types()
    {
        $options = $GLOBALS['WPUT_DUPLICATOR_OPTIONS'];

        $all_post_types = get_post_types(
            array(
                'public'    => true,
                '_builtin'   => false
            ),
            'objects'
        );
        ?>
        <input type="checkbox" name="select_all" id="select-all"><?php _e( 'Select All', WPUT_NAME ); ?><br /><br />
        <input type="checkbox" name="<?php echo WPUT_D_OPTIONS ?>[post_types][post]" value="post" <?php if( isset( $options['post_types']['post'] ) ) echo ' checked'; ?>>Post [post]<br />
        <input type="checkbox" name="<?php echo WPUT_D_OPTIONS ?>[post_types][page]" value="page" <?php if( isset( $options['post_types']['page'] ) ) echo ' checked'; ?>>Page [page]<br />
        <?php
        if( !empty( $all_post_types ) )
        {
            foreach( $all_post_types as $post_type )
            {
                if( !isset( $post_type->name ) ) continue;
                if( !isset( $post_type->label ) ) $label = 'Unknown';
                else $label = $post_type->label;
                $checked = '';
                if( isset( $options['post_types'][$post_type->name] ) ) $checked = ' checked';
                ?>                    
                <input type="checkbox" name="<?php echo WPUT_D_OPTIONS ?>[post_types][<?php echo $post_type->name; ?>]" value="<?php echo $post_type->name; ?>" <?php echo $checked; ?>><?php echo $label; ?> [<?php echo $post_type->name; ?>]<br />
                <?php
            }
        }
        ?>
        <p class="description"><?php _e( 'Select where to show duplicator options', WPUT_D_NAME ); ?></p>

        <?php
    }

    /**
     * Show all public post types to user to choose
     */
    public function delete()
    {
        $options = $GLOBALS['WPUT_DUPLICATOR_OPTIONS'];

        $all_post_types = get_post_types(
            array(
                'public'    => true,
                '_builtin'  => false
            ),
            'objects'
        );

        ?>
        <input type="checkbox" name="select_all" id="select-all"><?php _e( 'Select All', WPUT_NAME ); ?><br /><br />
        <input type="checkbox" name="<?php echo WPUT_D_OPTIONS; ?>[delete][post]" value="post" <?php if( isset( $options['delete']['post'] ) ) echo ' checked'; ?>>Post [post]<br />
        <input type="checkbox" name="<?php echo WPUT_D_OPTIONS; ?>[delete][page]" value="page" <?php if( isset( $options['delete']['page'] ) ) echo ' checked'; ?>>Page [page]<br />
        <?php
        if( !empty( $all_post_types ) )
        {
            foreach( $all_post_types as $post_type )
            {
                if( !isset( $post_type->name ) ) continue;
                if( !isset( $post_type->label ) ) $label = ucfirst( str_replace( '_', '', $post_type->name ) );
                else $label = $post_type->label;
                $checked = '';
                if( isset( $options['delete'][$post_type->name] ) ) $checked = ' checked';
                ?>                    
                <input type="checkbox" name="<?php echo WPUT_D_OPTIONS ?>[delete][<?php echo $post_type->name; ?>]" value="<?php echo $post_type->name; ?>" <?php echo $checked; ?>><?php echo $label; ?> [<?php echo $post_type->name; ?>]<br />
                <?php
            }
        }
        ?>
        <p class="description"><?php _e( 'Select where to show "Delete Permanenlty" option', WPUT_D_NAME ); ?></p>

        <?php
    }

    /**
     * Show settings page in WPUT settings
     */
    public function settings_duplicator_content()
    {
        if( $this->is_settings_active( WPUT_D_NAME ) && file_exists( WPUT_D_ADMIN . '/settings.php' ) ) include WPUT_D_ADMIN . '/settings.php';
    }

    /**
     * Show duplicator options tab in WPUT settings
     */
    public function settings_duplicator_tabs()
    {
        $wput_duplicator_active = '';
        if( $this->is_settings_active( WPUT_D_NAME ) ) $wput_duplicator_active = ' nav-tab-active';
        ?>
        <a href="<?php echo self_admin_url( 'admin.php?page=' . $_GET['page'] . '&tab=' . WPUT_D_NAME ); ?>" class="nav-tab <?php echo $wput_duplicator_active; ?>"><?php _e( 'Duplicator', WPUT_NAME ); ?></a>
        <?php
    }
}
endif;