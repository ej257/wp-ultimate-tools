<?php if ( ! defined( 'WPINC' ) ) { die( "Don't mess with us." ); }
/**
 * Create posts/pages duplicates or clones
 *
 * @since      1.0.0
 * @package    WP_Ultimate_Tools
 * @author     Goran Zivkovic
 */
if( !class_exists( 'WPUT_D_DUPLICATE' ) ) :
class WPUT_D_DUPLICATE extends WPUT_D_GLOBAL
{
    /**
     * Class clone name action property
     */
    private $action_clone;

    /**
     * Class new draft name action property
     */
    private $action_new_draft;

    /**
     * Class constructor
     */
    function __construct()
    {
        /**
         * Init properties
         */
        $this->action_clone = 'wput_d_clone';
        $this->action_new_draft = 'wput_d_new_draft';

        /**
         * Filters
         */
        $this->add_filter( 'post_row_actions', 'action_post_link', 10, 2 );
        $this->add_filter( 'page_row_actions', 'action_post_link', 10, 2 );
        /**
         * Actions
         */
        $this->add_action( 'admin_action_wput_d_clone', 'action_post' );
        $this->add_action( 'admin_action_wput_d_new_draft', 'action_post' );
        $this->add_action( 'admin_action_wput_delete', 'action_delete' );
    } 

    /**
     * Method that adds actions clone and new draft links to row
     */
    public function action_post_link( $actions, $post )
    {
        if( current_user_can( 'edit_posts' ) )
        {
            $WPUT_DUPLICATOR_OPTIONS = $GLOBALS[ 'WPUT_DUPLICATOR_OPTIONS' ];
            if( !isset( $WPUT_DUPLICATOR_OPTIONS['post_types'][ $post->post_type ] ) )
            {
                $actions['wput_d_clone'] = '<a href="' . wp_nonce_url('admin.php?action=wput_d_clone&post=' . $post->ID, $this->action_clone, 'wput_d_nonce_clone' ) . '" title="Clone" rel="permalink">'. __( 'Clone', WPUT_D_NAME ) . '</a>';
                $actions['wput_d_new_draft'] = '<a href="' . wp_nonce_url('admin.php?action=wput_d_new_draft&post=' . $post->ID, $this->action_new_draft, 'wput_d_nonce_draft' ) . '" title="New Draft" rel="permalink">'. __( 'New Draft', WPUT_D_NAME ) . '</a>';
            }
            if( isset( $WPUT_DUPLICATOR_OPTIONS['delete'][ $post->post_type ] ) && $post->post_status !== 'trash' )
            {
                $actions['wput_delete'] = '<a style="color:red;" href="' . wp_nonce_url('admin.php?action=wput_delete&post=' . $post->ID, 'wput_delete', 'wput_delete' ) . '" title="Delete Permanently" rel="permalink">'. __( 'Delete Permanently', WPUT_D_NAME ) . '</a>';
            }
        }
        return $actions;
    }

    /**
     * Method that performs post/page cloning
     */
    public function action_post()
    {
        if( !( isset( $_GET['post'] ) || isset( $_GET['page'] ) || isset( $_GET['action'] ) ) && ( $_REQUEST['action'] === 'wput_d_clone_post' || $_REQUEST['action'] === 'wput_d_new_draft' ) ) wp_die( __( 'Sorry can\'t perform action.', WPUT_D_NAME ) );
        if( isset( $_GET['wput_d_clone'] ) )
        {
            if( !wp_verify_nonce( $_GET['wput_d_clone'], $this->action_clone ) ) return false;
        }
        elseif( isset( $_GET['wput_d_new_draft'] ) )
        {
            if( !wp_verify_nonce( $_GET['wput_d_new_draft'], $this->action_new_draft ) ) return false;
        }

        global $wpdb;

        
        $post_id = isset( $_GET['post'] ) ? absint( $_GET['post'] ) : absint( $_POST['post'] );

        $post = get_post( $post_id );

        if( empty( $post ) ) wp_die( __( 'Sorry we could not find original post: ', WPUT_D_NAME ) . $post_id );

        // Determine author. Draft = current user | Clone = original user
        if( $_REQUEST['action'] === 'wput_d_clone' ) 
        {
            $author = $post->post_author;
            $status = $post->post_status;
            $title = $post->post_title . ' - CLONE';
        }
        else
        {
            $current_user = wp_get_current_user();
            $author = $current_user->ID;
            $status = 'draft';
            $title = $post->post_title . ' - Draft';
        }

        $args = array(
            'comment_status' => $post->comment_status,
			'ping_status'    => $post->ping_status,
			'post_author'    => $author,
			'post_content'   => $post->post_content,
			'post_excerpt'   => $post->post_excerpt,
			'post_name'      => $post->post_name,
			'post_parent'    => $post->post_parent,
			'post_password'  => $post->post_password,
			'post_status'    => $status,
			'post_title'     => $title,
			'post_type'      => $post->post_type,
			'to_ping'        => $post->to_ping,
			'menu_order'     => $post->menu_order
        );
        
        /**
         * Insert new draft or clone into WP DB
         */
        $new_post_id = wp_insert_post( $args );

        $taxonomies = get_object_taxonomies( $post->post_type ); // Returns array of taxonomy names for post type, ex array("category", "post_tag");

        /**
         * Get all current post terms and set them to new one draft or clone
         */
        foreach ($taxonomies as $taxonomy) 
        {
			$post_terms = wp_get_object_terms( $post_id, $taxonomy, array('fields' => 'slugs') );
			wp_set_object_terms( $new_post_id, $post_terms, $taxonomy, false );
		}

        /**
         * Duplicate all post meta
         */
        $post_meta_infos = $wpdb->get_results("SELECT meta_key, meta_value FROM $wpdb->postmeta WHERE post_id=$post_id");
        if ( empty( $post_meta_infos ) ) return false; 

        $sql_query = "INSERT INTO $wpdb->postmeta (post_id, meta_key, meta_value) ";
        foreach ( $post_meta_infos as $meta_info ) 
        {
            $meta_key = $meta_info->meta_key;
            if( $meta_key[0] == '_' ) continue;
            $meta_value = addslashes( $meta_info->meta_value );
            $sql_query_sel[]= "SELECT $new_post_id, '$meta_key', '$meta_value'";
        }
        $sql_query.= implode( " UNION ALL ", $sql_query_sel );
        $wpdb->query( $sql_query );

        /**
         * Redirect user to check or modify new clone or draft
         */
        wp_redirect( self_admin_url( 'post.php?action=edit&post=' . $new_post_id ) );
    } 

    /**
     * Method that performs deleting
     */
    public function action_delete()
    {
        if( !( isset( $_GET['post'] ) || isset( $_GET['page'] ) || isset( $_GET['action'] ) ) && $_REQUEST['action'] === 'wput_delete' ) wp_die( __( 'Sorry can\'t perform action.', WPUT_D_NAME ) );
    
        if( !wp_verify_nonce( $_GET['wput_delete'], 'wput_delete' ) ) 
        {
            wp_die( __( 'Direct access is forbidden!', WPUT_D_NAME ) );
            return false;
        }

        $post_id = isset( $_GET['post'] ) ? absint( $_GET['post'] ) : absint( $_POST['post'] );

        $post = get_post( $post_id );

        wp_delete_post( $post_id, true );

        /**
         * Redirect user to edit menu
         */
        $redirect_url = '';
        if( $post->post_type !== 'post' ) $redirect_url = '?post_type=' . $post->post_type;
        wp_redirect( self_admin_url( 'edit.php' . $redirect_url ) );
    }
}
endif;