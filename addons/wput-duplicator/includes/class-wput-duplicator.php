<?php if ( ! defined( 'WPINC' ) ) { die( "Don't mess with us." ); }
/**
 * Main Duplicator Addon Class
 *
 * @since      1.0.0
 * @package    WP_Ultimate_Tools
 * @author     Goran Zivkovic
 */
if( !class_exists( 'WPUT_D_INIT' ) ) :
class WPUT_D_INIT extends WPUT_D_GLOBAL
{
    /**
     * Run all addon functions 
     */
    public function run_duplicator()
    {
        /**
         * Duplicator system
         */
        if( file_exists( WPUT_D_INCLUDES . '/class-wput-duplicator-duplicate.php' ) )
        {
            require_once WPUT_D_INCLUDES . '/class-wput-duplicator-duplicate.php';
            if( class_exists( 'WPUT_D_DUPLICATE' ) )
            {
                new WPUT_D_DUPLICATE;
            }
        }

        /**
         * Admin
         */
        if( file_exists( WPUT_D_INCLUDES . '/class-wput-duplicator-admin.php' ) )
        {
            require_once WPUT_D_INCLUDES . '/class-wput-duplicator-admin.php';
            if( class_exists( 'WPUT_D_ADMIN' ) )
            {
                new WPUT_D_ADMIN;
            }
        }

        do_action( WPUT_D_HOOK . 'class_load' );
    }

    /**
     * Load addon text domain for translations
     */
    public function load_textdomain()
    {
        
    }
}
endif;