<?php if ( ! defined( 'WPINC' ) ) { die( "Don't mess with us." ); }
/**
 * Addon for duplicating WP posts and pages
 *
 * @since      1.0.0
 * @version    1.0.0
 * @package    WP_Ultimate_Tools
 * @author     Goran Zivkovic
 */

/**
 * Defines
 */
// Main plugin file
if ( ! defined( 'WPUT_D_FILE' ) )				define( 'WPUT_D_FILE', __FILE__ );
// Current plugin version
if ( ! defined( 'WPUT_D_VERSION' ) )			define( 'WPUT_D_VERSION', '1.0.0');
// Plugin root
if ( ! defined( 'WPUT_D_ROOT' ) )				define( 'WPUT_D_ROOT', rtrim(plugin_dir_path(WPUT_D_FILE), '/') );
// Includes directory
if ( ! defined( 'WPUT_D_INCLUDES' ) )			define( 'WPUT_D_INCLUDES', WPUT_D_ROOT . '/includes' );
// Includes directory
if ( ! defined( 'WPUT_D_ADMIN' ) )			    define( 'WPUT_D_ADMIN', WPUT_D_ROOT . '/admin' );
// Plugin URL root
if ( ! defined( 'WPUT_D_URL' ) )				define( 'WPUT_D_URL', rtrim(plugin_dir_url( WPUT_D_FILE ), '/') );
// Assets URL
if ( ! defined( 'WPUT_D_ASSETS' ) )			    define( 'WPUT_D_ASSETS', WPUT_D_URL.'/assets' );
// Plugin name
if ( ! defined( 'WPUT_D_NAME' ) )				define( 'WPUT_D_NAME', 'wput-duplicator');
// Plugin session prefix (controlled by version)
if ( ! defined( 'WPUT_D_PREFIX' ) )			    define( 'WPUT_D_PREFIX', 'wp_ut_'.preg_replace("/[^0-9]/Ui",'',WPUT_D_VERSION).'_');
// Plugin prefix for hooks and others
if( ! defined( 'WPUT_D_HOOK' ) )                define( 'WPUT_D_HOOK', 'wput_duplicator_' );
// Plugin options name
if( ! defined( 'WPUT_D_OPTIONS' ) )             define( 'WPUT_D_OPTIONS', 'wput_duplicator_options' );

if( !class_exists( 'WPUT_D_GLOBAL' ) && !class_exists( 'WPUT_D_INIT' ) )
{
    /**
     * Include hook class
     */
    include_once WPUT_D_INCLUDES . '/class-wput-duplicator-global.php';

    if( class_exists( 'WPUT_D_GLOBAL' ) )
    {
        /**
         * Call hook class and make all options global
         */
        $hook = new WPUT_D_GLOBAL;

        $options = $hook->get_option( WPUT_D_OPTIONS );
        if( empty( $options ) )
        {
            $hook->update_option( WPUT_D_OPTIONS, $hook->default_options_duplicator );
            $options = $hook->get_option( WPUT_D_OPTIONS );   
        }
        $GLOBALS['WPUT_DUPLICATOR_OPTIONS'] = $options;
        /**
         * Include main addon class
         */
        include_once WPUT_D_INCLUDES . '/class-wput-duplicator.php';

        if( class_exists( 'WPUT_D_INIT' ) )
        {
            class WPUT_D_LOAD extends WPUT_D_INIT
            {
                function __construct()
                {
                    //$this->add_action( WPUT_HOOK . 'activate', 'activate_duplicator' );
                    //$this->add_action( WPUT_HOOK . 'deactivate', 'deactivate_duplicator' );
                    do_action( WPUT_D_HOOK . 'init' );
                    $this->run_duplicator();
                }
            }
        }
        /**
         * Clear CPU and RAM
         */
        $hook = NULL;
    }

    /**
     * Everything is probably loaded
     */
    function wput_duplicator()
    {
        if( class_exists( 'WPUT_D_LOAD' ) ) return new WPUT_D_LOAD;
    }

    /**
     * Let's start the show
     */
    wput_duplicator();
}