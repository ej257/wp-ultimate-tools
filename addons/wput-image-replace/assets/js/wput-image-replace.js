/**
 * Show media for replacing image
 * 
 * This MUST be pure JS otherwise we wont be able to open editor while inside editor
 */
function test() {
    var frame = wp.media({
        title: WPUT_IMG.title,
        multiple: false
    }).open()
    .on('select', function () {

        var uploaded_file = frame.state().get('selection').first();
        var accept = [ 'jpg', 'jpeg', 'png', 'gif' ];
        if( accept.indexOf( uploaded_file.attributes.subtype ) >= 0 )
        {
            jQuery( '#wput_replace_image' ).val( uploaded_file.attributes.id );
            if( jQuery( '#wput_replace_image' ).closest( '.media-modal' ).length )
            {
                jQuery( '#wput_replace_image' ).change();
                location.href = WPUT_IMG.upload;
            }
            else jQuery( '#wput_replace_image' ).closest( 'form' ).submit(); // This probably wont work but let's give it try
        }
    });
};