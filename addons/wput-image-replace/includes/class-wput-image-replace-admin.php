<?php if ( ! defined( 'WPINC' ) ) { die( "Don't mess with us." ); }
/**
 * Admin side of addon
 *
 * @since      1.0.0
 * @package    WP_Ultimate_Tools
 * @author     Goran Zivkovic
 */
if( !class_exists( 'WPUT_IMG_ADMIN' ) ) :
class WPUT_IMG_ADMIN extends WPUT_IMG_GLOBAL
{
    /**
     * Class consturctor
     */
    function __construct()
    {
        /**
         * Actions
         */
        $this->add_action( 'admin_enqueue_scripts', 'register_js' );

        /**
         * Filters
         */
    }

    /**
     * Register JS
     */
    public function register_js()
    {
        wp_enqueue_media();
        wp_register_script( WPUT_IMG_NAME . '-js', WPUT_IMG_ASSETS . '/js/wput-image-replace.js', array( 'jquery' ), WPUT_IMG_VERSION, true );
        wp_localize_script(
            WPUT_IMG_NAME . '-js', 
            'WPUT_IMG', 
            array(
                'title'     => __( 'Replace Image', WPUT_IMG_NAME ),
                'upload'    => self_admin_url( 'upload.php' ),
            )
        );
        wp_enqueue_script( WPUT_IMG_NAME . '-js' );
    }
}
endif;