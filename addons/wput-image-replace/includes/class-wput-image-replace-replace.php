<?php if ( ! defined( 'WPINC' ) ) { die( "Don't mess with us." ); }
/**
 * Image replace system
 *
 * @since      1.0.0
 * @package    WP_Ultimate_Tools
 * @author     Goran Zivkovic
 */
if( !class_exists( 'WPUT_IMG_REPLACE' ) ) :
class WPUT_IMG_REPLACE extends WPUT_IMG_GLOBAL
{
    /**
     * Class constructor
     */
    function __construct()
    {
        /**
         * Actions
         */
        $this->add_action( 'edit_attachment', 'replace_image' );

        /**
         * Filters
         */
        $this->add_filter( 'attachment_fields_to_edit', 'image_fields' );
        $this->add_filter( 'wp_calculate_image_srcset', 'image_srcset' );
        $this->add_filter( 'wp_get_attachment_image_src', 'image_src' );
        $this->add_filter( 'wp_prepare_attachment_for_js', 'image_js' );
    }

    /**
     * Add custom fields to image editor
     */
    public function image_fields( $form_fields )
    {  
        wp_enqueue_media(); // Just in case
        $form_fields['wput_image_replace'] = array(
            'label'         => __( 'Replace Image', WPUT_IMG_NAME ),
            'input'         => 'html',
            'html'          => '<button type="button" onclick="test()" id="wput_replace_image_button" class="button-secondary button-large">Replace Image</button>
                <input type="hidden" id="wput_replace_image" name="wput_replace_image" />         
                <p><strong>Warning:</strong> Replacing this image with another one will permanently delete the current image file and replacment image will overwrite this one. Old data will be saved.</p>   
            '
        );

        return $form_fields;
    }

    /**
     * Change img url in srcset to prevent caching
     */
    public function image_srcset( $sources )
    {
        if( is_admin() )
        {
            foreach( $sources as $size => $source )
            {
                $source['url'] = $this->add_url_time( $source['url'] );
                $sources[ $size ] = $source; // Save changes
            }
        }

        return $sources;
    }

    /**
     * Change img url to prevent caching
     */
    public function image_src( $image_data )
    {
        if( is_admin() && !empty( $image_data[0] ) ) $image_data[0] = $this->add_url_time( $image_data[0] );
        
        return $image_data;
    }

    /**
     * Change img url for JS prepared data to prevent caching
     */
    public function image_js( $response )
    {
        if( is_admin() )
        {
            $response['url'] = $this->add_url_time( $response['url'] );
            if( isset( $response['sizes'] ) && is_array( $response['sizes'] ) )
            {
                foreach( $response['sizes'] as $size_name => $size )
                {
                    $response['sizes'][ $size_name ]['url'] = $this->add_url_time( $size['url'] );
                }
            }
        }

        return $response;
    }

    /**
     * Replace image without data loss
     */
    public function replace_image( $post_id )
    {
        if( isset( $_POST['wput_replace_image'] ) && is_numeric( $_POST['wput_replace_image'] ) )
        {
            $replace_img_id = absint( $_POST['wput_replace_image'] );
            $upload_dir = wp_upload_dir();

            $file = path_join( $upload_dir['basedir'], get_post_meta( $replace_img_id, '_wp_attached_file', true ) );

            if( !file_exists( $file ) ) return false; // In JS we check for file type
            
            $this->delete_attachment( $post_id );

            $old_file = path_join( $upload_dir['basedir'], get_post_meta( $post_id, '_wp_attached_file', true ) );

            if( !file_exists( $old_file ) ) mkdir( dirname( $old_file ), 0777, true );
            copy( $file, $old_file );

            $meta_data = wp_generate_attachment_metadata( $post_id, $old_file );
            wp_update_attachment_metadata( $post_id, $meta_data );
            wp_delete_attachment( $replace_img_id,  true );
        }
    } 

    /**
     * Add time query param in url to prevent caching
     */
    public function add_url_time( $url )
    {
        $url = add_query_arg( 't', time(), $url );

        return $url;
    }

    /**
     * Delete attachment
     * 
     * wp-includes/post.php && wp-includes/functions.php - Source
     */
    public function delete_attachment( $post_id )
    {
        $meta = wp_get_attachment_metadata( $post_id );
        $backup_sizes = get_post_meta( $post_id, '_wp_attachment_backup_sizes', true );
        $file = get_attached_file( $post_id );

        if ( is_multisite() ) delete_transient( 'dirsize_cache' );

        $uploadpath = wp_get_upload_dir();

        if ( !empty( $meta['thumb'] ) ) 
        {
            $thumbfile = str_replace( basename( $file ), $meta['thumb'], $file );
            $thumbfile = apply_filters( 'wp_delete_file', $thumbfile );
            @unlink( path_join( $uploadpath['basedir'], $thumbfile ) );
        }

        if ( isset( $meta['sizes'] ) && is_array( $meta['sizes'] ) ) 
        {
            foreach ( $meta['sizes'] as $size => $sizeinfo ) 
            {
                $intermediate_file = str_replace( basename( $file ), $sizeinfo['file'], $file );
                $intermediate_file = apply_filters( 'wp_delete_file', $intermediate_file );
                @unlink( path_join( $uploadpath['basedir'], $intermediate_file ) );
            }
        }

        if ( is_array( $backup_sizes ) ) 
        {
            foreach ( $backup_sizes as $size ) 
            {
                $del_file = path_join( dirname( $meta['file'] ), $size['file'] );
                $del_file = apply_filters( 'wp_delete_file', $del_file );
                @unlink( path_join( $uploadpath['basedir'], $del_file ) );
            }
        }
        wp_delete_file( $file );
    }
}
endif;