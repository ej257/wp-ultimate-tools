<?php if ( ! defined( 'WPINC' ) ) { die( "Don't mess with us." ); }
/**
 * Main Image Replace Addon Class
 *
 * @since      1.0.0
 * @package    WP_Ultimate_Tools
 * @author     Goran Zivkovic
 */
if( !class_exists( 'WPUT_IMG_INIT' ) ) :
class WPUT_IMG_INIT extends WPUT_IMG_GLOBAL
{
    /**
     * Run all addon functions 
     */
    public function run_image_replace()
    {
        /**
         * Admin
         */
        if( file_exists( WPUT_IMG_INCLUDES . '/class-wput-image-replace-admin.php' ) )
        {
            require_once WPUT_IMG_INCLUDES . '/class-wput-image-replace-admin.php';
            if( class_exists( 'WPUT_IMG_ADMIN' ) ) new WPUT_IMG_ADMIN; 
        }

        /**
         * Image replace system
         */
        if( file_exists( WPUT_IMG_INCLUDES . '/class-wput-image-replace-replace.php' ) )
        {
            require_once WPUT_IMG_INCLUDES . '/class-wput-image-replace-replace.php';
            if( class_exists( 'WPUT_IMG_REPLACE' ) ) new WPUT_IMG_REPLACE;
        }

        do_action( WPUT_IMG_HOOK . 'class_load' );
    }

    /**
     * Load addon text domain for translations
     */
    public function load_textdomain()
    {
        
    }
}
endif;