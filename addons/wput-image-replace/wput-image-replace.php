<?php if ( ! defined( 'WPINC' ) ) { die( "Don't mess with us." ); }
/**
 * Addon for replacing images without lossing any data
 *
 * @since      1.0.0
 * @version    1.0.0
 * @package    WP_Ultimate_Tools
 * @author     Goran Zivkovic
 */

/**
 * Defines
 */
// Main plugin file
if ( ! defined( 'WPUT_IMG_FILE' ) )				define( 'WPUT_IMG_FILE', __FILE__ );
// Current plugin version
if ( ! defined( 'WPUT_IMG_VERSION' ) )			define( 'WPUT_IMG_VERSION', '1.0.0');
// Plugin root
if ( ! defined( 'WPUT_IMG_ROOT' ) )				define( 'WPUT_IMG_ROOT', rtrim(plugin_dir_path(WPUT_IMG_FILE), '/') );
// Includes directory
if ( ! defined( 'WPUT_IMG_INCLUDES' ) )			define( 'WPUT_IMG_INCLUDES', WPUT_IMG_ROOT . '/includes' );
// Includes directory
if ( ! defined( 'WPUT_IMG_ADMIN' ) )			    define( 'WPUT_IMG_ADMIN', WPUT_IMG_ROOT . '/admin' );
// Plugin URL root
if ( ! defined( 'WPUT_IMG_URL' ) )				define( 'WPUT_IMG_URL', rtrim(plugin_dir_url( WPUT_IMG_FILE ), '/') );
// Assets URL
if ( ! defined( 'WPUT_IMG_ASSETS' ) )			    define( 'WPUT_IMG_ASSETS', WPUT_IMG_URL.'/assets' );
// Plugin name
if ( ! defined( 'WPUT_IMG_NAME' ) )				define( 'WPUT_IMG_NAME', 'wput-duplicator');
// Plugin session prefix (controlled by version)
if ( ! defined( 'WPUT_IMG_PREFIX' ) )			    define( 'WPUT_IMG_PREFIX', 'wp_ut_'.preg_replace("/[^0-9]/Ui",'',WPUT_IMG_VERSION).'_');
// Plugin prefix for hooks and others
if( ! defined( 'WPUT_IMG_HOOK' ) )                define( 'WPUT_IMG_HOOK', 'wput_image_replace_' );
// Plugin options name
if( ! defined( 'WPUT_IMG_OPTIONS' ) )             define( 'WPUT_IMG_OPTIONS', 'wput_image_replace_options' );

if( !class_exists( 'WPUT_IMG_GLOBAL' ) && !class_exists( 'WPUT_IMG_INIT' ) )
{
    /**
     * Include hook class
     */
    include_once WPUT_IMG_INCLUDES . '/class-wput-image-replace-global.php';

    if( class_exists( 'WPUT_IMG_GLOBAL' ) )
    {
        /**
         * Call hook class
         */
        $hook = new WPUT_IMG_GLOBAL;

        /**
         * Include main addon class
         */
        include_once WPUT_IMG_INCLUDES . '/class-wput-image-replace.php';

        if( class_exists( 'WPUT_IMG_INIT' ) )
        {
            class WPUT_IMG_LOAD extends WPUT_IMG_INIT
            {
                function __construct()
                {
                    //$this->add_action( WPUT_HOOK . 'activate', 'activate_duplicator' );
                    //$this->add_action( WPUT_HOOK . 'deactivate', 'deactivate_duplicator' );
                    do_action( WPUT_IMG_HOOK . 'init' );
                    $this->run_image_replace();
                }
            }
        }
        /**
         * Clear CPU and RAM
         */
        $hook = NULL;
    }

    /**
     * Everything is probably loaded
     */
    function wput_image_replace()
    {
        if( class_exists( 'WPUT_IMG_LOAD' ) ) return new WPUT_IMG_LOAD;
    }

    /**
     * Let's start the show
     */
    wput_image_replace();
}