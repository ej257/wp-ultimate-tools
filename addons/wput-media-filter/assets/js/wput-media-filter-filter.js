(function($){
    /**
     * When user change taxonomy hide term select to prevent misunderstanding
     */
    $( 'select#wput_filter_tax' ).on( 'change', function( e ) {
        $( 'select#wput_filter_term' ).remove();
        e.preventDefault();
    });
    
})(jQuery || window.jQuery || Zepto || window.Zepto);