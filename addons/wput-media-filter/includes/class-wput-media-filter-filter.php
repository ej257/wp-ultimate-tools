<?php if ( ! defined( 'WPINC' ) ) { die( "Don't mess with us." ); }
/**
 * Media filter system
 * 
 * Whole SQL code is copied from internet with minnor upgrades
 *
 * @since      1.0.0
 * @package    WP_Ultimate_Tools
 * @author     Goran Zivkovic
 */

if( !class_exists( 'WPUT_MEDIA_FILTER_FILTER' ) ) :
class WPUT_MEDIA_FILTER_FILTER extends WPUT_MEDIA_FILTER_GLOBAL
{
    /**
     * Class constructor
     */
    function __construct()
    {
        /**
         * Actions
         */
        $this->add_action( 'restrict_manage_posts', 'add_custom_media_filter' );
        $this->add_action( 'admin_enqueue_scripts', 'register_js' );

        /**
         * Filters 
         */
        $this->add_filter( 'posts_join', 'filter_join' );
        $this->add_filter( 'posts_groupby', 'filter_groupby' );
    }

    /**
     * Register JS
     */
    public function register_js()
    {
        if( !$this->is_media_page() ) return false;

        wp_register_script( WPUT_MEDIA_NAME . '-filter-js', WPUT_MEDIA_ASSETS . '/js/wput-media-filter-filter.js', array( 'jquery' ), WPUT_MEDIA_VERSION, true );
        wp_enqueue_script( WPUT_MEDIA_NAME . '-filter-js' );
    } 

    /**
     * Add JOIN statment in main query for querying posts with filter request
     */
    public function filter_join( $sql )
    {
        if( !$this->is_media_page() ) return $sql;

        global $wpdb;

        $taxonomy = $this->get_selected_taxonomy();
        $term = $this->get_selected_term();

        if( empty( $taxonomy ) ) return $sql;

        $taxonomy = $wpdb->prepare( " AND $wpdb->term_taxonomy.taxonomy = %s ", $taxonomy );
        $term = !empty( $term_id ) ? $wpdb->prepare( " AND $wpdb->terms.term_id = %d ", $term ) : " ";

        $sql .= " ";
        $sql .= "INNER JOIN $wpdb->term_relationships ON ( $wpdb->posts.post_parent = $wpdb->term_relationships.object_id ) ";
        $sql .= "INNER JOIN $wpdb->term_taxonomy ON ( $wpdb->term_relationships.term_taxonomy_id = $wpdb->term_taxonomy.term_taxonomy_id ) ";
        $sql .= $taxonomy;
        $sql .= "INNER JOIN $wpdb->terms ON ( $wpdb->terms.term_id = $wpdb->term_taxonomy.term_id ) ";
        $sql .= $term;

        return $sql;
    }

    /**
     * Add GROUP_BY statment in main query for querying posts with filter request
     */
    public function filter_groupby( $groupby )
    {
        if( !$this->is_media_page() ) return $groupby;

        $taxonomy = $this->get_selected_taxonomy();

        if( empty( $taxonomy ) ) return $groupby;

        global $wpdb;

        $groupby = $wpdb->posts . '.ID';

	    return $groupby;
    }

    /**
     * Add select menus for filtering
     */
    public function add_custom_media_filter()
    {
        if( !$this->is_media_page() ) return false;

        $taxonomies = $this->wput_get_taxonomies();

        $exclude_tax = $this->get_excluded_taxonomies();

        $selected_tax = $this->get_selected_taxonomy();

        if( empty( $taxonomies ) ) return false;

        $html = sprintf( '<select name="wput_filter_tax" id="wput_filter_tax" class="postform"><option value="">%s</option>', __( 'All Taxonomies', WPUT_MEDIA_NAME ) );
        foreach( $taxonomies as $i => $taxonomy )
        {
            if( isset( $exclude_tax[ $taxonomy->name ] ) ) continue;

            $tax = get_taxonomy( $taxonomy->name );
            $html .= sprintf( '<option value="%s" class="level-0" %s>%s</option>', $taxonomy->name, selected( $taxonomy->name, $selected_tax, false ), $tax->labels->name );
        }
        $html .= '</select>';

        if( $selected_tax )
        {
            $terms = $this->wput_get_terms( $selected_tax );

            if( !empty( $terms ) )
            {
                $selected_term = $this->get_selected_term();
                $html .= sprintf( '<select name="wput_filter_term" id="wput_filter_term" class="postform"><option value="">%s</option>', __( 'All Terms', WPUT_MEDIA_NAME ) );
                foreach( $terms as $i => $term )
                {
                    $html .= sprintf( '<option value="%s" class="level-0" %s>%s</option>', $term->term_id, selected( $term->term_id, $selected_term, false ), $term->name );
                }
                $html .= '</select>';
            }
        } 

        echo $html;
    }

    /**
     * Get all wanted taxonomies determinated by all filters
     */
    public function wput_get_taxonomies()
    {
        global $wpdb;

        $date = $this->get_sql_date();
        $attachment = $this->get_sql_attachment_filter();
        $search = $this->get_sql_search_query();

        $sql = "
            SELECT
                tt.taxonomy AS 'name',
                COUNT( DISTINCT( child.ID ) ) AS 'total'
            FROM $wpdb->posts AS child
                LEFT JOIN $wpdb->posts AS parent ON parent.ID = child.post_parent
                LEFT JOIN $wpdb->term_relationships AS tr ON tr.object_id = parent.ID
                INNER JOIN $wpdb->term_taxonomy AS tt ON tt.term_taxonomy_id = tr.term_taxonomy_id
            WHERE 1 = 1
            $date
            $attachment
            $search
            AND child.post_type = 'attachment'
            AND ( child.post_status = 'inherit' OR child.post_status = 'private' )
            GROUP BY tt.taxonomy
            ORDER BY tt.taxonomy ASC
		";

	    $taxonomies = $wpdb->get_results( $sql );

	    return $taxonomies;
    }

    /**
     * Get taxonomy terms wanted by filter
     */
    public function wput_get_terms( $taxonomy )
    {
        if( empty( $taxonomy ) ) return false;

        global $wpdb;

        $taxonomy = $wpdb->prepare( " AND tt.taxonomy = %s ", $taxonomy );
        $date = $this->get_sql_date();
        $attachment = $this->get_sql_attachment_filter();
        $search = $this->get_sql_search_query();

        $sql = "
            SELECT
                tt.taxonomy AS 'taxonomy',
                t.name AS 'name',
                t.term_id AS 'term_id',
                COUNT( DISTINCT( child.ID ) ) AS 'total'
            FROM $wpdb->posts AS child
                LEFT JOIN $wpdb->posts AS parent ON parent.ID = child.post_parent
                LEFT JOIN $wpdb->term_relationships AS tr ON tr.object_id = parent.ID
                INNER JOIN $wpdb->term_taxonomy AS tt ON tt.term_taxonomy_id = tr.term_taxonomy_id
                    $taxonomy
                LEFT JOIN $wpdb->terms AS t ON t.term_id = tt.term_id
            WHERE 1 = 1
            $date
            $attachment
            $search
            AND child.post_type = 'attachment'
            AND ( child.post_status = 'inherit' OR child.post_status = 'private' )
            GROUP BY t.term_id
            ORDER BY t.name ASC
        ";
        
        return $wpdb->get_results( $sql );
    }

    /**
     * Get selected taxonomy from filter request
     */
    public function get_selected_taxonomy()
    {
        return $this->get( 'wput_filter_tax' );
    }

    /**
     * Get selected term from filter request
     */
    public function get_selected_term()
    {
        return $this->get( 'wput_filter_term', 'int' );
    }

    /**
     * Get date from filter request
     */
    public function get_date()
    {
        $date = $this->get( 'm', 'int' );

        if( $date > 0 ) return array( 'year' => substr( $date, 0, 4 ), 'month' => substr( $date, -2 ) );

        return false;
    }

    /**
     * Get date in sql format 
     */
    public function get_sql_date()
    {
        global $wpdb;
        $selected = $this->get_date();

        $year = esc_sql( $selected['year'] );
        $month = esc_sql( $selected['month'] );

        return $selected ? $wpdb->prepare( " AND YEAR(child.post_date) = %d AND MONTH(chiild.post_date) = %d ", $year, $month ) : ' ';
    }

    /**
     * Get attachment filter in sql format
     */
    public function get_sql_attachment_filter()
    {
        $filter = $this->get( 'attachment-filter', 'encode' );

        if ( $filter == 'detached' ) return " AND child.post_parent = 0 ";

        $filter = urldecode( $filter );
        $filter = array_map( 'esc_sql', explode( ':', $filter ) );

        if( count( $filter ) != 2 ) return "";

        global $wpdb;

        $val = $filter[1] . '/%';

        return $wpdb->prepare( " AND ( child.post_mime_type LIKE %s ) ", $val );
    }

    /**
     * Get search query in sql format
     */
    public function get_sql_search_query()
    {
        $search = $this->get( 's', 'encode' );
        $search = trim( $search );

        if( empty( $search ) ) return ' ';

        global $wpdb;

        $search = sprintf( '%%%s%%', $wpdb->esc_like( $search ) );

        return $wpdb->prepare( " AND ( ( child.post_title LIKE %s ) OR ( child.post_content LIKE %s ) ) ", $search, $search );
    }

    /**
     * Check if we are on media upload page with list mode
     */
    public function is_media_page()
    {
        $screen = get_current_screen();

        if( ( !isset( $screen->id ) || $screen->id != 'upload' ) || ( isset( $_GET['mode'] ) && $_GET['mode'] == 'grid' ) ) return false;

        return true;
    }

    /**
     * Get all excluded taxonomies
     */
    public function get_excluded_taxonomies()
    {
        $taxonomies = array(
            'nav_menu'                  => 'nav_menu',
            'link_category'             => 'link_category',
            'post_format'               => 'post_format',
            'product_shipping_class'    => 'product_shipping_class',
        );

        return $taxonomies;
    }
}
endif;