<?php if ( ! defined( 'WPINC' ) ) { die( "Don't mess with us." ); }
/**
 * Main Auto Login Addon Class
 *
 * @since      1.0.0
 * @package    WP_Ultimate_Tools
 * @author     Goran Zivkovic
 */

if( !class_exists( 'WPUT_MEDIA_FILTER_INIT' ) ) :
class WPUT_MEDIA_FILTER_INIT extends WPUT_MEDIA_FILTER_GLOBAL
{
    /**
     * Run all plugin functions
     */
    public function run_media_filter()
    {
        /**
         * Media filter system
         */
        if( file_exists( WPUT_MEDIA_INCLUDES . '/class-wput-media-filter-filter.php' ) )
        {
            require_once WPUT_MEDIA_INCLUDES . '/class-wput-media-filter-filter.php';
            if( class_exists( 'WPUT_MEDIA_FILTER_FILTER' ) ) new WPUT_MEDIA_FILTER_FILTER;
        }
    }
}
endif;