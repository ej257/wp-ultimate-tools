<?php if ( ! defined( 'WPINC' ) ) { die( "Don't mess with us." ); }
/**
 * Addon for media filter
 *
 * @since      1.0.0
 * @version    1.0.0
 * @package    WP_Ultimate_Tools
 * @author     Goran Zivkovic
 */

/**
 * Defines
 */
// Main plugin file
if ( ! defined( 'WPUT_MEDIA_FILE' ) )				define( 'WPUT_MEDIA_FILE', __FILE__ );
// Current plugin version
if ( ! defined( 'WPUT_MEDIA_VERSION' ) )			define( 'WPUT_MEDIA_VERSION', '1.0.0');
// Plugin root
if ( ! defined( 'WPUT_MEDIA_ROOT' ) )				define( 'WPUT_MEDIA_ROOT', rtrim(plugin_dir_path(WPUT_MEDIA_FILE), '/') );
// Includes directory
if ( ! defined( 'WPUT_MEDIA_INCLUDES' ) )			define( 'WPUT_MEDIA_INCLUDES', WPUT_MEDIA_ROOT . '/includes' );
// Includes directory
if ( ! defined( 'WPUT_MEDIA_ADMIN' ) )			    define( 'WPUT_MEDIA_ADMIN', WPUT_MEDIA_ROOT . '/admin' );
// Plugin URL root
if ( ! defined( 'WPUT_MEDIA_URL' ) )				define( 'WPUT_MEDIA_URL', rtrim(plugin_dir_url( WPUT_MEDIA_FILE ), '/') );
// Assets URL
if ( ! defined( 'WPUT_MEDIA_ASSETS' ) )            define( 'WPUT_MEDIA_ASSETS', WPUT_MEDIA_URL.'/assets' );
// Plugin name
if ( ! defined( 'WPUT_MEDIA_NAME' ) )				define( 'WPUT_MEDIA_NAME', 'wput-media-filter');
// Plugin session prefix (controlled by version)
if ( ! defined( 'WPUT_MEDIA_PREFIX' ) )		    define( 'WPUT_MEDIA_PREFIX', 'wput_media_filter_'.preg_replace("/[^0-9]/Ui",'',WPUT_MEDIA_VERSION).'_');
// Plugin prefix for hooks and others
if( ! defined( 'WPUT_MEDIA_HOOK' ) )               define( 'WPUT_MEDIA_HOOK', 'wput_media_filter_' );
// Plugin options name
if( ! defined( 'WPUT_MEDIA_OPTIONS' ) )            define( 'WPUT_MEDIA_OPTIONS', 'wput_media_filter_options' );

if( !class_exists( 'WPUT_MEDIA_GLOBAL' ) && !class_exists( 'WPUT_MEDIA_INIT' ) )
{
    /**
     * Include hook class
     */
    include_once WPUT_MEDIA_INCLUDES . '/class-wput-media-filter-global.php';

    if( class_exists( 'WPUT_MEDIA_FILTER_GLOBAL' ) )
    {
        /**
         * Call hook class and make all options global
         */
        /*$hook = new WPUT_MEDIA_GLOBAL;

        $options = $hook->get_option( WPUT_MEDIA_OPTIONS ); 
        if( empty( $options ) )
        {
            $hook->update_option( WPUT_MEDIA_OPTIONS, $hook->default_options_auto_login );
            $options = $hook->get_option( WPUT_MEDIA_OPTIONS );
        }
        $GLOBALS['WPUT_MEDIA'] = $options;
        /**
         * Include main addon class
         */
        include_once WPUT_MEDIA_INCLUDES . '/class-wput-media-filter.php';

        if( class_exists( 'WPUT_MEDIA_FILTER_INIT' ) )
        {
            class WPUT_MEDIA_FILTER_LOAD extends WPUT_MEDIA_FILTER_INIT
            {
                function __construct()
                {
                    //$this->add_action( WPUT_HOOK . 'activate', 'activate_auto_login' );
                    //$this->add_action( WPUT_HOOK . 'deactivate', 'deactivate_auto_login' );
                    do_action( WPUT_MEDIA_HOOK . 'init' );
                    $this->run_media_filter();
                }
            }
        }
        /**
         * Clear CPU and RAM
         */
        $hook = NULL;
    }

    /**
     * Everything is probably loaded
     */
    function wput_media_filter()
    {
        if( class_exists( 'WPUT_MEDIA_FILTER_LOAD' ) ) return new WPUT_MEDIA_FILTER_LOAD;
    }

    /**
     * Let's start the show
     */
    wput_media_filter();
}