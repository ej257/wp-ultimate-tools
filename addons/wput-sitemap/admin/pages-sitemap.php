<?php if ( ! defined( 'WPINC' ) ) { die( "Don't mess with us." ); }
/**
 * Settings Page
 *
 * @since      1.0.0
 * @package    WP_Ultimate_Tools
 * @author     Goran Zivkovic
 */

if( isset( $_POST['submit'] ) )
{
    WPUT_SM_GLOBAL::save_sitemap_meta();
}
?>
<div id="post-body" class="metabox-holder columns-1">
    <div id="postbox-container" class="postbox-container-1">
        <form action="<?php echo self_admin_url( 'admin.php?page=' . $_GET['page'] . '&tab=' . $_GET['tab'] . '&sitemap=' . $_GET['sitemap'] ); ?>" method="post">
            <div class="meta-box-sortables ui-sortable" id="normal-sortables">
                <div class="postbox " id="itsec_get_started">
                    <h3 class="hndle"><span><?php _e( 'Pages sitemap params', WPUT_SM_NAME ); ?></span></h3>
                    <div class="inside">
                        <table class="wp-list-table widefat fixed posts">
                            <thead>
                                <tr>
                                    <td><strong><?php _e( 'Priority', WPUT_SM_NAME ); ?></strong></td>
                                    <td><strong><?php _e( 'Interval', WPUT_SM_NAME ); ?></strong></td>
                                    <td><strong><?php _e( 'Title [name]', WPUT_SM_NAME ); ?></strong></td>
                                </tr>
                            </thead>
                            <tfoot>
                                <tr>
                                    <td><strong><?php _e( 'Priority', WPUT_SM_NAME ); ?></strong></td>
                                    <td><strong><?php _e( 'Interval', WPUT_SM_NAME ); ?></strong></td>
                                    <td><strong><?php _e( 'Title [name]', WPUT_SM_NAME ); ?></strong></td>
                                </tr>
                            </tfoot>
                            <tbody>
                            <?php
                                $pages = get_pages( array( 'post_status' => 'publish', 'post_type' => 'page' ) );
                                
                                if( !empty( $pages) )
                                {
                                    foreach( $pages as $i => $page )
                                    {
                                        echo '<tr>';
                                        $priority = get_post_meta( $page->ID, 'priority', true );
                                        $interval = get_post_meta( $page->ID, 'interval', true );
                                        if( empty( $priority ) && rtrim( home_url(), '/' ) === rtrim( get_permalink( $page ), '/' ) ) $priority = 1.00;
                                        elseif( empty( $priority ) ) $priority = 0.90;
                                        if( empty( $interval ) ) $interval = 'daily';
                                        printf( '<td>%s</td>', WPUT_SM_GLOBAL::get_page_sitemap_params( $page->ID, $priority, 'priority' ) );
                                        printf( '<td>%s</td>', WPUT_SM_GLOBAL::get_page_sitemap_params( $page->ID, $interval, 'interval' ) );
                                        printf( '<td>%s [%s]</td>', ucfirst( $page->post_title ), $page->post_name );
                                        echo '</tr>';
                                    }
                                }
                                else
                                {
                                    printf( '<tr><td colspan=3>%s</td></tr>', __( 'No pages found.', WPUT_SM_NAME ) );
                                }
                            ?> 
                            </tbody>
                        </table>
                        <a href="<?php echo self_admin_url( 'admin.php?page=wp-ultimate-tools-page-settings&tab=wput-sitemap' ); ?>" class="button button-primary"><?php _e( 'Back to main sitemap options.', WPUT_SM_NAME ); ?></a>
                        <?php submit_button(); ?>
                        <div class="clear"></div>
                    </div>
                </div>
            </div>
        </form>
    </div>
</div>