<?php if ( ! defined( 'WPINC' ) ) { die( "Don't mess with us." ); }
/**
 * Admin side of addon
 *
 * @since      1.0.0
 * @package    WP_Ultimate_Tools
 * @author     Goran Zivkovic
 */
if( !class_exists( 'WPUT_SM_ADMIN' ) ) :
class WPUT_SM_ADMIN extends WPUT_SM_GLOBAL
{
    /**
     * Class Constructor
     */
    function __construct()
    {
        /**
         * Actions
         */
        $this->add_action( 'admin_init', 'settings_init' );
        $this->add_action( WPUT_HOOK . 'settings', 'settings_sitemap_content' );
        $this->add_action( WPUT_HOOK . 'settings_tabs', 'settings_sitemap_tabs' );
    
        /**
         * Filters
         */
    }
    /**
     * Image options and settings
     */
    public function settings_init()
    {
        /**
         * Register options_group and options
         */
        register_setting( 
            WPUT_SM_NAME . '-settings', 
            WPUT_SM_OPTIONS,
            array(
                'default'               => $this->deafult_options_sitemap,
            )
        );

        /**
         * Add new settings section
         * 
         * add_settings_section($id, $title, $callback, $page)
         */
        add_settings_section(
            WPUT_SM_NAME . '-general-options',
            apply_filters( WPUT_SM_HOOK . 'general_settings_title', __( 'General Options', WPUT_SM_NAME ) ),
            array( &$this, 'wput_sitemap_image_general_section' ),
            WPUT_SM_NAME . '-page-settings'
        );

        /**
         * Add settings fields to section
         * 
         * add_settings_field($id, $title, $callback, $page, $section, $args);
         */
        add_settings_field(
            'sitemap_params',
            apply_filters( WPUT_SM_HOOK . 'sitemap_title', __( 'Set params', WPUT_SM_NAME ) ),
            array( &$this, 'sitemap_params' ),
            WPUT_SM_NAME . '-page-settings',
            WPUT_SM_NAME . '-general-options'
        );

        /**
         * Add settings fields to section
         * 
         * add_settings_field($id, $title, $callback, $page, $section, $args);
         */
        add_settings_field(
            'sitemap_pages',
            apply_filters( WPUT_SM_HOOK . 'sitemap_pages_title', __( 'Set params for pages', WPUT_SM_NAME ) ),
            array( &$this, 'sitemap_pages' ),
            WPUT_SM_NAME . '-page-settings',
            WPUT_SM_NAME . '-general-options'
        );

        /**
         * Add settings fields to section
         * 
         * add_settings_field($id, $title, $callback, $page, $section, $args);
         */
        add_settings_field(
            'sitemap_file',
            apply_filters( WPUT_SM_HOOK . 'sitemap_file_title', __( 'Sitemap File', WPUT_SM_NAME ) ),
            array( &$this, 'sitemap_file' ),
            WPUT_SM_NAME . '-page-settings',
            WPUT_SM_NAME . '-general-options'
        );
    }

    /**
     * General section callback
     */
    public function wput_sitemap_image_general_section( $args )
    {
        return false;
    }

    /**
     * Show settings page in WPUT settings
     */
    public function settings_sitemap_content()
    {
        if( $this->is_settings_active( WPUT_SM_NAME )  ) 
        {
            if( isset( $_GET['sitemap'] ) && $_GET['sitemap'] == 'pages' )
            {
                if( file_exists( WPUT_SM_ADMIN . '/pages-sitemap.php' ) ) include WPUT_SM_ADMIN . '/pages-sitemap.php';
            }
            elseif( file_exists( WPUT_SM_ADMIN . '/settings.php' ) ) include WPUT_SM_ADMIN . '/settings.php';
        }
    }

    /**
     * Show image options tab in WPUT settings
     */
    public function settings_sitemap_tabs()
    {
        $wput_image_active = '';
        if( $this->is_settings_active( WPUT_SM_NAME ) ) $wput_image_active = ' nav-tab-active';
        ?>
        <a href="<?php echo self_admin_url( 'admin.php?page=' . $_GET['page'] . '&tab=' . WPUT_SM_NAME ); ?>" class="nav-tab <?php echo $wput_image_active; ?>"><?php _e( 'Sitemap', WPUT_NAME ); ?></a>
        <?php
    }

    /**
     * Sitemap taxonomies and post types options callback
     */
    public function sitemap_params()
    {

        /**
         * Reset sitemap options to default
         */
        $button_text = __( 'Reset To Defaults', WPUT_SM_NAME );
        if( class_exists( 'WC_product' ) ) $button_text = __( 'Optimize for e-store', WPUT_SM_NAME );
        $button = sprintf( '<input type="submit" id="reset" name="reset" value="%s" class="button button-primary">', $button_text );
        ?>
        <table class="wp-list-table widefat fixed posts">
            <thead>
                <tr>
                    <td><strong><?php _e( 'Priority', WPUT_SM_NAME ); ?></strong></td>
                    <td><strong><?php _e( 'Interval', WPUT_SM_NAME ); ?></strong></td>
                    <td><strong><?php _e( 'Name', WPUT_SM_NAME ); ?></strong></td>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <td><strong><?php _e( 'Priority', WPUT_SM_NAME ); ?></strong></td>
                    <td><strong><?php _e( 'Interval', WPUT_SM_NAME ); ?></strong></td>
                    <td><strong><?php _e( 'Name', WPUT_SM_NAME ); ?></strong></td>
                </tr>
            </tfoot>
            <tbody>
            <?php
                foreach( $this->deafult_options_sitemap as $key => $options )
                {
                    if( strpos( $key, 'product' ) !== false && !class_exists( 'WC_product' ) ) continue;
                    echo '<tr>';
                    printf( '<td>%s</td>', $this->get_sitemap_priority( $key ) );
                    printf( '<td>%s</td>', $this->get_sitemap_interval( $key ) );
                    if( $key === 'unlisted' ) $key = __( 'Unlisted post_types/taxonomies', WPUT_SM_NAME );
                    printf( '<td>%s</td>', ucfirst( $key ) );
                    echo '</tr>';
                }
            ?>
            </tbody>
        </table>
        <p class="description"><strong><?php _e( 'Sitemap will be created/updated after every post/page/taxonomy save/update/delete.' ); ?></strong></p>
        <?php
        //echo $button;
    }

    /**
     * Sitemap for pages options callback
     */
    public function sitemap_pages()
    {
        ?>
        <a href="<?php echo self_admin_url( 'admin.php?page='. $_GET['page'] .'&tab='. $_GET['tab'] .'&sitemap=pages' ); ?>" class="button button-primary"><?php _e( 'Set params', WPUT_SM_NAME ); ?></a>
        <?php
    }

    /**
     * Sitemap file
     */
    public function sitemap_file()
    {
        if( file_exists( ABSPATH . '/sitemap.xml' ) )
        {
        ?>
            <a href="<?php echo rtrim( home_url(), '/' ) . '/sitemap.xml'; ?>" target="_blank" class="button button-primary"><?php _e( 'See sitemap', WPUT_SM_NAME ); ?></a>
        <?php
        }
        else
        {
            ?>
            <p><strong><?php _e( 'Sitemap file does not exists yet.', WPUT_SM_NAME ); ?></strong></p>
            <?php
        }
    }
}
endif;