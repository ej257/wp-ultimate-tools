<?php if ( ! defined( 'WPINC' ) ) { die( "Don't mess with us." ); }
/**
 * Sitemap creator
 *
 * @since      1.0.0
 * @package    WP_Ultimate_Tools
 * @author     Goran Zivkovic
 */
if( !class_exists( 'WPUT_SM_CREATOR' ) ) :
class WPUT_SM_CREATOR extends WPUT_SM_GLOBAL
{
    /**
     * Class constructor
     */
    function __construct()
    {
        /**
         * Actions
         */
        // Create Sitemap
		$this->add_action( 'publish_post', 'create_sitemap_callback', 1, 0 );
		$this->add_action( 'publish_page', 'create_sitemap_callback', 1, 0 );
		$this->add_action( 'publish_product', 'create_sitemap_callback', 1, 0 );
		$this->add_action( 'delete_post', 'create_sitemap_callback', 1, 0 );
		$this->add_action( 'edit_post', 'create_sitemap_callback', 1, 0 );
		$this->add_action( 'edit_term', 'create_sitemap_callback', 1, 0 );
		$this->add_action( 'create_term', 'create_sitemap_callback', 1, 0 );
        $this->add_action( 'delete_term', 'create_sitemap_callback', 1, 0 );
        /**
         * Filters
         */
    }

    /**
	 * Sitemap Creator
	 */
	public function create_sitemap_callback()
	{
		$this->create_sitemap();
	}
	
}
endif;