<?php if ( ! defined( 'WPINC' ) ) { die( "Don't mess with us." ); }
/**
 * Hooks, actions and other helpers
 *
 * @since      1.0.0
 * @package    WP_Ultimate_Tools
 * @author     Goran Zivkovic
 */
if( !class_exists( 'WPUT_SM_GLOBAL' ) && class_exists( 'WPUT_Global' ) ) :
class WPUT_SM_GLOBAL extends WPUT_Global
{
    /**
     * All available options
     */
    public $deafult_options_sitemap = array(
        'post' => array(
           'priority'   => 0.7,
           'interval'   => 'daily',
        ),
        'product' => array(
            'priority'  => 0.85,
            'intreval'  => 'daily',
        ),
        'category' => array(
            'priority'  => 0.6,
            'interval'  => 'monthly',
        ),
        'post_tag' => array(
            'priority'  => 0.6,
            'interval'  => 'monthly',
        ),
        'product_cat' => array(
            'priority'  => 0.8,
            'interval'  => 'monthly',
        ),
        'product_tag' => array(
            'priority'  => 0.75,
            'interval'  => 'monthly',
        ),
        'product_attributes'    => array(
            'priority'  => 0.75,
            'interval'  => 'monthly'
        ),
        'unlisted'     => array(
            'priority'  => 0.64,
            'interval'  => 'monthly',
        ),
    );

    /**
     * Class constructor
     */
    function __construct()
    {
        
    }

    public static function get_page_sitemap_params( $page_id, $value, $type )
    {
        $intervals = array(
            'always',
            'hourly',
            'daily',
            'weekly',
            'monthly',
            'yearly',
        );

        if( $type === 'priority' )
        {
            $html = sprintf( '<select name="wput_page_priority:%d">', $page_id );
            for( $i = 0.10; $i < 1.01; $i += 0.01 )
            {
                $selected = '';
                if( round( $i, 2 ) == round( $value, 2 ) ) $selected = ' selected';

                $html .= sprintf( '<option value="%.02f" %s>%.02f</option>', $i, $selected, $i );
            }
            $html .= '</select>';
        }
        elseif( $type === 'interval' )
        {
            $html = sprintf( '<select name="wput_page_interval:%d">', $page_id );
            foreach( $intervals as $i=> $interval )
            {
                $selected = '';
                if( $value === $interval ) $selected = ' selected';
                $html .= sprintf( '<option value="%s" %s>%s</option>', $interval, $selected, ucfirst( $interval ) );
            }
            $html .= '</select>';
        }

        return $html;
    }

    /**
     * Get sitemap priority
     */
    public function get_sitemap_priority( $key )
    {
        $options = $GLOBALS['WPUT_SITEMAP_OPTIONS'];
        if( !isset( $options[ $key ] ) ) return false;
    
        $html = sprintf( '<select name="%s[%s][priority]">', WPUT_SM_OPTIONS, $key );
        for( $i = 0.10; $i < 1.00; $i += 0.01 )
        {
            $selected = '';
            if( isset( $options[ $key ]['priority'] ) && (float)$options[ $key ]['priority'] == round( $i, 2 ) ) $selected = ' selected';

            $html .= sprintf( '<option value="%.02f" %s>%.02f</option>', $i, $selected, $i );
        }

        $html .= '</select>';

        return $html;
    }

    /**
     * Get sitemap interval
     */
    public function get_sitemap_interval( $key )
    {
        $options = $GLOBALS['WPUT_SITEMAP_OPTIONS'];
        if( !isset( $options[ $key ] ) ) return false;
    
        $html = sprintf( '<select name="%s[%s][interval]">', WPUT_SM_OPTIONS, $key );
        $intervals = array(
            'always',
            'hourly',
            'daily',
            'weekly',
            'monthly',
            'yearly',
        );
        foreach( $intervals as $i=> $interval )
        {
            $selected = '';
            if( isset( $options[ $key ]['interval'] ) && $options[ $key ]['interval'] === $interval ) $selected = ' selected';
            $html .= sprintf( '<option value="%s" %s>%s</option>', $interval, $selected, ucfirst( $interval ) );
        }

        $html .= '</select>';

        return $html;
    }

    /**
     * Save meta data for sitemap
     */
    public static function save_sitemap_meta()
    {
        foreach( $_POST as $key => $val )
        {
            if( strpos( $key, 'wput_page' ) === false ) continue;

            $page_id = explode( ':', $key );

            if( strpos( $key, 'priority' ) !== false ) update_post_meta( absint( $page_id[1] ), 'priority', round( (float)$val, 2) );
            elseif( strpos( $key, 'interval' ) !== false ) update_post_meta( absint( $page_id[1] ), 'interval', $val );
        }
    }

    /**
     * Sitemap creator
     */
    public function create_sitemap()
    {
		$options = $GLOBALS['WPUT_SITEMAP_OPTIONS'];
        $postsForSitemap = get_posts(array(
			'numberposts' => -1,
			'orderby' => 'modified',
			'post_status' => 'publish',
        //  'post_type'  => array( 'post', 'page', 'product' ),
			'post_type'  => get_post_types( array('public'   => true) ),
			'order'    => 'DESC',
		));
		
		$today = date("c");
	
		$sitemap = '<?xml version="1.0" encoding="UTF-8"?><?xml-stylesheet type="text/xsl" href="' . WPUT_SM_ASSETS . '/xml/xml-sitemap.xsl"?>';
		$sitemap .= '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:image="http://www.google.com/schemas/sitemap-image/1.1">';
	
		foreach( $postsForSitemap as $post )
		{
			setup_postdata( $post );
	
			$postdate = date("c", strtotime($post->post_modified));
			$page_permalink = get_permalink( $post->ID );
			
			
			if(function_exists('wc_get_page_id'))
			{
				if(in_array($post->ID, array(
					wc_get_page_id( 'myaccount' ),
					$this->get_url_to_postid(wc_customer_edit_account_url()), //wc_get_page_id( 'edit_address' ),
					wc_get_page_id( 'cart' ),
					wc_get_page_id( 'checkout' ),					
					wc_get_page_id( 'view_order' ),
				), true) !== false) continue;
			}
			
			//if(get_post_meta( $post->ID, 'ge_seo_sitemap_exclude', true ) == 'on') continue;
	
			$sitemap .= '<url>'.
			  '<loc>' . esc_url( $page_permalink ). '</loc>';
			
			// Add image
			if( $image = get_the_post_thumbnail_url($post) ){
				$sitemap .= '<image:image>'.
					'<image:loc>' .  esc_url( $image ) . '</image:loc>' .
					'<image:title>' .  esc_attr( esc_html( get_the_title( $post->ID ) ) ). '</image:title>' .
				'</image:image>';
			}
			// Add gallery
			if(class_exists('WC_product'))
			{
				if( $post->post_type == 'product' )
				{
					$product = new WC_product($post->ID);
					if($gallery = $product->get_gallery_attachment_ids())
					{
						foreach( $gallery as $attachment_id ) {
							if($image_url = wp_get_attachment_url( $attachment_id ))
							{
								$sitemap .= '<image:image>'.
									'<image:loc>' .  esc_url( $image_url ) . '</image:loc>' .
								'</image:image>';
							}
						}
					}
				}
			}
			// Page property
			if( $post->post_type == 'page' )
			{
				$interval = get_post_meta( $post->ID, 'interval', true );
				if( empty( $interval ) ) $interval = 'weekly';
				$priority = get_post_meta( $post->ID, 'priority', true );
				if( empty( $priority ) ) $priority = 0.50;
			}
			else
			{
				if( isset( $options[ $post->post_type ] ) ) $interval = $options[ $post->post_type ]['interval'];
				elseif( isset( $options['unlisted'] ) ) $interval = $options['unlisted']['interval'];
				else $interval = 'weekly';

				if( isset( $options[ $post->post_type ] ) ) $priority = $options[ $post->post_type ]['priority'];
				elseif( isset( $options['unlisted'] ) ) $priority = $options['unlisted']['priority'] ;
				else $priority = 0.50; 
			}
			$sitemap .= '<lastmod>' . $postdate . '</lastmod>' .
				'<changefreq>'. $interval . '</changefreq>' .
				'<priority>'. $priority .'</priority>'.
			'</url>';
		}
		
		foreach(array(
			'category',
			'post_tag',
			'product_cat',
			'product_tag',
			'product_attributes',
			'product_brand',
		) as $taxonomy)
		{
			$remove_exists = array();
			$all_categories = get_categories(array(
				'taxonomy'     	=> $taxonomy,
				'orderby'      	=> 'name',
				'order'			=> 'ASC',
				'hide_empty'   	=> 1,
				'show_numbers' 	=> 0,
				'hierarchical' 	=> 0,
			));
			
			if( !empty( $all_categories ) )
			{
				foreach( $all_categories as $cat )
				{
					if(in_array($cat->name, $remove_exists, true) !== false) continue;
					$remove_exists[] = $cat->name;
					
					$cat_link = get_category_link( $cat->term_id );
					if( !empty($cat_link) )
					{
						
						
						$sitemap .= '<url>'.
						  '<loc>' . esc_url( $cat_link ). '</loc>' .
						  '<lastmod>' . $today . '</lastmod>' .
						  '<changefreq>' . ( isset( $options[ $cat->name ] ) ? $options[ $cat->name ]['interval'] : ( isset( $options['unlisted'] ) ? $options['unlisted']['interval'] : 'weekly' ) )  . '</changefreq>' .
						  '<priority>'. ( isset( $options[ $cat->name ] ) ? $options[ $cat->name ]['priority'] : ( isset( $options['unlisted'] ) ? $options['unlisted']['priority'] : 0.5 ) ) .'</priority>'.
						 '</url>';
					}
				}
			}
		}
	
		$sitemap .= '</urlset>';
		if(function_exists('fopen'))
		{
			$fp = fopen( ABSPATH . 'sitemap.xml', 'w' );
			fwrite( $fp, $sitemap );
            fclose( $fp );
        }
	}
    
}
endif;