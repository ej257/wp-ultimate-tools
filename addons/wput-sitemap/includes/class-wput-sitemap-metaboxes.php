<?php if ( ! defined( 'WPINC' ) ) { die( "Don't mess with us." ); }
/**
 * Metaboxes
 *
 * @since      1.0.0
 * @package    WP_Ultimate_Tools
 * @author     Goran Zivkovic
 */

if( !class_exists( 'WPUT_SM_METABOX' ) ) :
class WPUT_SM_METABOX extends WPUT_SM_GLOBAL
{
    /**
     * Class consturcor
     */
    function __construct()
    {
        /**
         * Actions
         */
        $this->add_action( 'add_meta_boxes', 'add_metaboxes' );
        $this->add_action( 'save_post', 'save_metabox_data' );

        /**
         * Filters
         */
    }

    /**
     * Add metaboxes
     */
    public function add_metaboxes()
    {
        global $post;
        /**
         * Set sitemap params
         */
        if( $post->post_type === 'page' && $post->post_status === 'publish' )
        {
            add_meta_box( 
                'wput_sm_sitemap', 
                __( 'Pages sitemap params' ), 
                array( &$this, 'post_sitemap_params' )
            );
        }
    }

    /**
     * Metabox: Pages sitemap params
     */
    public function post_sitemap_params()
    {
        global $post;
        ?>
        <table class="wp-list-table widefat fixed posts">
            <thead>
                <tr>
                    <td><strong><?php _e( 'Priority', WPUT_SM_NAME ); ?></strong></td>
                    <td><strong><?php _e( 'Interval', WPUT_SM_NAME ); ?></strong></td>
                    <td><strong><?php _e( 'Title [name]', WPUT_SM_NAME ); ?></strong></td>
                </tr>
            </thead>
            <tfoot>
                <tr>
                    <td><strong><?php _e( 'Priority', WPUT_SM_NAME ); ?></strong></td>
                    <td><strong><?php _e( 'Interval', WPUT_SM_NAME ); ?></strong></td>
                    <td><strong><?php _e( 'Title [name]', WPUT_SM_NAME ); ?></strong></td>
                </tr>
            </tfoot>
            <tbody>
            <?php
                $priority = get_post_meta( $post->ID, 'priority', true );
                $interval = get_post_meta( $post->ID, 'interval', true );
                if( empty( $priority ) && rtrim( home_url(), '/' ) === rtrim( get_permalink( $post ), '/' ) ) $priority = 1.00;
                elseif( empty( $priority ) ) $priority = 0.90;
                if( empty( $interval ) ) $interval = 'daily';
                printf( '<td>%s</td>', WPUT_SM_GLOBAL::get_page_sitemap_params( $post->ID, $priority, 'priority' ) );
                printf( '<td>%s</td>', WPUT_SM_GLOBAL::get_page_sitemap_params( $post->ID, $interval, 'interval' ) );
                printf( '<td>%s [%s]</td>', $post->post_title, $post->post_name );
            ?>
        </table>
        <?php
    }

    /**
     * Save metabox data
     */
    public function save_metabox_data()
    {
        WPUT_SM_GLOBAL::save_sitemap_meta();
    }
}
endif;