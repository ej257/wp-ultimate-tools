<?php if ( ! defined( 'WPINC' ) ) { die( "Don't mess with us." ); }
/**
 * Main Sitemap Addon Class
 *
 * @since      1.0.0
 * @package    WP_Ultimate_Tools
 * @author     Goran Zivkovic
 */
if( !class_exists( 'WPUT_SM_INIT' ) ) :
class WPUT_SM_INIT extends WPUT_SM_GLOBAL
{
    /**
     * Run all addon functions
     */
    public function run_sitemap()
    {
        /**
         * Admin
         */
        if( file_exists( WPUT_SM_INCLUDES . '/class-wput-sitemap-admin.php' ) )
        {
            require_once WPUT_SM_INCLUDES . '/class-wput-sitemap-admin.php';
            if( class_exists( 'WPUT_SM_ADMIN' ) ) new WPUT_SM_ADMIN;
        }

        /**
         * Sitemap creator
         */
        if( file_exists( WPUT_SM_INCLUDES . '/class-wput-sitemap-creator.php' ) )
        {
            require_once WPUT_SM_INCLUDES . '/class-wput-sitemap-creator.php';
            if( class_exists( 'WPUT_SM_CREATOR' ) ) new WPUT_SM_CREATOR;
        }

        /**
         * Metaboxes
         */
        if( file_exists( WPUT_SM_INCLUDES . '/class-wput-sitemap-metaboxes.php' ) )
        {
            require_once WPUT_SM_INCLUDES . '/class-wput-sitemap-metaboxes.php';
            if( class_exists( 'WPUT_SM_METABOX' ) ) new WPUT_SM_METABOX;
        }
    }

    /**
     * Load addon text domain for translations
     */
    public function load_textdomain()
    {
        
    }
}
endif;