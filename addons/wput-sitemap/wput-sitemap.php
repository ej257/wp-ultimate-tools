<?php if ( ! defined( 'WPINC' ) ) { die( "Don't mess with us." ); }
/**
 * Addon for creating sitemap
 *
 * @since      1.0.0
 * @version    1.0.0
 * @package    WP_Ultimate_Tools
 * @author     Goran Zivkovic
 */

/**
 * Defines
 */
// Main plugin file
if ( ! defined( 'WPUT_SM_FILE' ) )				define( 'WPUT_SM_FILE', __FILE__ );
// Current plugin version
if ( ! defined( 'WPUT_SM_VERSION' ) )			define( 'WPUT_SM_VERSION', '1.0.0');
// Plugin root
if ( ! defined( 'WPUT_SM_ROOT' ) )				define( 'WPUT_SM_ROOT', rtrim(plugin_dir_path(WPUT_SM_FILE), '/') );
// Includes directory
if ( ! defined( 'WPUT_SM_INCLUDES' ) )			define( 'WPUT_SM_INCLUDES', WPUT_SM_ROOT . '/includes' );
// Includes directory
if ( ! defined( 'WPUT_SM_ADMIN' ) )			define( 'WPUT_SM_ADMIN', WPUT_SM_ROOT . '/admin' );
// Plugin URL root
if ( ! defined( 'WPUT_SM_URL' ) )				define( 'WPUT_SM_URL', rtrim(plugin_dir_url( WPUT_SM_FILE ), '/') );
// Assets URL
if ( ! defined( 'WPUT_SM_ASSETS' ) )           define( 'WPUT_SM_ASSETS', WPUT_SM_URL.'/assets' );
// Plugin name
if ( ! defined( 'WPUT_SM_NAME' ) )				define( 'WPUT_SM_NAME', 'wput-sitemap');
// Plugin session prefix (controlled by version)
if ( ! defined( 'WPUT_SM_PREFIX' ) )			define( 'WPUT_SM_PREFIX', 'wput_sitemap_'.preg_replace("/[^0-9]/Ui",'',WPUT_SM_VERSION).'_');
// Plugin prefix for hooks and others
if( ! defined( 'WPUT_SM_HOOK' ) )              define( 'WPUT_SM_HOOK', 'wput_sitemap_' );
// Plugin options name
if( ! defined( 'WPUT_SM_OPTIONS' ) )           define( 'WPUT_SM_OPTIONS', 'wput_sitemap_options' );

if( !class_exists( 'WPUT_SM_GLOBAL' ) && !class_exists( 'WPUT_SM_INIT' ) )
{
    /**
     * Include hook class
     */
    include_once WPUT_SM_INCLUDES . '/class-wput-sitemap-global.php';

    if( class_exists( 'WPUT_SM_GLOBAL' ) )
    {
        /**
         * Call hook class and make all options global
         */
        $hook = new WPUT_SM_GLOBAL;

        $options = $hook->get_option( WPUT_SM_OPTIONS ); 
        if( empty( $options ) )
        {
            $hook->update_option( WPUT_SM_OPTIONS, $hook->deafult_options_sitemap );
            $options = $hook->get_option( WPUT_SM_OPTIONS );
        }
        $GLOBALS['WPUT_SITEMAP_OPTIONS'] = $options;
        /**
         * Include main addon class
         */
        include_once WPUT_SM_INCLUDES . '/class-wput-sitemap.php';

        if( class_exists( 'WPUT_SM_INIT' ) )
        {
            class WPUT_SM_LOAD extends WPUT_SM_INIT
            {
                function __construct()
                {
                    //$this->add_action( WPUT_HOOK . 'activate', 'activate_image' );
                    //$this->add_action( WPUT_HOOK . 'deactivate', 'deactivate_image' );
                    do_action( WPUT_SM_HOOK . 'init' );
                    $this->run_sitemap();
                }
            }
        }
        /**
         * Clear CPU and RAM
         */
        $hook = NULL;
    }

    /**
     * Everything is probably loaded
     */
    function wput_sitemap()
    {
        if( class_exists( 'WPUT_SM_LOAD' ) ) return new WPUT_SM_LOAD;
    }

    /**
     * Let's start the show
     */
    wput_sitemap();
}