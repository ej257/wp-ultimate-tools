(function($) {
    /**
     * Image uploader
     */
    (function( $$ )
    {
        if( $($$) )
        {
            $($$).each(function () {
				$uploader = $$;
				$( '#image-button', $uploader ).click( function (e) {
					e.preventDefault();
					var file = wp.media({
						title: 'Taxonomy Image',
						multiple: false
					}).open()
					.on('select', function (e) {
						var uploaded_file = file.state().get('selection').first();
						var file_url = uploaded_file.attributes.url;

                        var accept = [ 'jpg', 'jpeg', 'png', 'gif' ];
                        if( accept.indexOf( uploaded_file.attributes.subtype ) >= 0 )
                        {
                            $( '.image-url', $uploader).val( file_url );
                        }
                        else
                        {
                            $( '.image-url', $uploader ).val( '' );
                        }
					});
                });
                
                $( '#remove-image-button', $uploader ).click( function (e) {
                    e.preventDefault();
                    $( '.image-url', $uploader ).val( '' );
                });
			});
        }
    }('#wput-taxonomy-image'));
})(jQuery || window.jQuery || Zepto || window.Zepto);