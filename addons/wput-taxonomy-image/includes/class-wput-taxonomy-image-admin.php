<?php if ( ! defined( 'WPINC' ) ) { die( "Don't mess with us." ); }
/**
 * Admin side of addon
 *
 * @since      1.0.0
 * @package    WP_Ultimate_Tools
 * @author     Goran Zivkovic
 */
if( !class_exists( 'WPUT_TAX_ADMIN' ) ) :
class WPUT_TAX_ADMIN extends WPUT_TAX_GLOBAL
{
    /**
     * Class Constructor
     */
    function __construct()
    {
        /**
         * Actions
         */
        $this->add_action( 'admin_init', 'settings_init' );
        $this->add_action( WPUT_HOOK . 'settings', 'settings_taxonomy_content' );
        $this->add_action( WPUT_HOOK . 'settings_tabs', 'settings_taxonomy_tabs' );

        /**
         * Filters
         */
    }

    /**
     * Image options and settings
     */
    public function settings_init()
    {
        /**
         * Register options_group and options
         */
        register_setting( 
            WPUT_TAX_NAME . '-settings', 
            WPUT_TAX_OPTIONS,
            array(
                'default'               => $this->deafult_options_tax_image,
                'sanitize_callback'     => array( &$this, 'sanitize_options' )
            )
        );

        /**
         * Add new settings section
         * 
         * add_settings_section($id, $title, $callback, $page)
         */
        add_settings_section(
            WPUT_TAX_NAME . '-general-options',
            apply_filters( WPUT_TAX_HOOK . 'general_settings_title', __( 'General Options', WPUT_TAX_NAME ) ),
            array( &$this, 'wput_taxonomy_image_general_section' ),
            WPUT_TAX_NAME . '-page-settings'
        );

        /**
         * Add settings fields to section
         * 
         * add_settings_field($id, $title, $callback, $page, $section, $args);
         */
        add_settings_field(
            'taxonomies',
            apply_filters( WPUT_TAX_HOOK . 'taxonomies_title', __( 'Show image options in', WPUT_TAX_NAME ) ),
            array( &$this, 'taxonomies' ),
            WPUT_TAX_NAME . '-page-settings',
            WPUT_TAX_NAME . '-general-options'
        );

        /**
         * Add settings fields to section
         * 
         * add_settings_field($id, $title, $callback, $page, $section, $args);
         */
        add_settings_field(
            'image_size',
            apply_filters( WPUT_TAX_HOOK . 'image_size_title', __( 'Image size', WPUT_TAX_NAME ) ),
            array( &$this, 'image_size' ),
            WPUT_TAX_NAME . '-page-settings',
            WPUT_TAX_NAME . '-general-options'
        );
    }

    /**
     * General section callback
     */
    public function wput_taxonomy_image_general_section( $args )
    {
        return false;
    }

    /**
     * Show settings page in WPUT settings
     */
    public function settings_taxonomy_content()
    {
        if( $this->is_settings_active( WPUT_TAX_NAME ) && file_exists( WPUT_TAX_ADMIN . '/settings.php' ) ) include WPUT_TAX_ADMIN . '/settings.php';
    }

    /**
     * Show image options tab in WPUT settings
     */
    public function settings_taxonomy_tabs()
    {
        $wput_image_active = '';
        if( $this->is_settings_active( WPUT_TAX_NAME ) ) $wput_image_active = ' nav-tab-active';
        ?>
        <a href="<?php echo self_admin_url( 'admin.php?page=' . $_GET['page'] . '&tab=' . WPUT_TAX_NAME ); ?>" class="nav-tab <?php echo $wput_image_active; ?>"><?php _e( 'Taxonomy Image', WPUT_NAME ); ?></a>
        <?php
    }

    /**
     * Show all taxonomies to user to choose
     */
    public function taxonomies()
    {
        $options = $GLOBALS['WPUT_TAXONOMY_IMAGE_OPTIONS'];
        
        $disabled_taxonomies = array('nav_menu', 'link_category', 'post_format');
        $taxonomies = get_taxonomies( array( 'public' => true ), 'objects');
        ?>
        <input type="checkbox" name="select_all" id="select-all"><?php _e( 'Select All', WPUT_NAME ); ?><br /><br />
        <?php
        foreach( $taxonomies as $tax )
        {
            if( in_array( $tax->name, $disabled_taxonomies, true ) ) continue;
            $checked = '';
            if( isset( $options['taxonomies'][$tax->name] ) ) $checked = ' checked';
            ?>
            <input type="checkbox" name="<?php echo WPUT_TAX_OPTIONS; ?>[taxonomies][<?php echo $tax->name; ?>]" value="<?php echo $tax->name; ?>" <?php echo $checked ?>><?php echo $tax->label; ?> [<?php echo $tax->name; ?>]<br />
            <?php
        }
        ?>
        <p class="description"><?php _e( 'Select where to show taxonomy image options', WPUT_TAX_NAME ); ?></p>
        <?php
    }

    /**
     * Options for width and height
     */
    public function image_size()
    {
        $options = $GLOBALS['WPUT_TAXONOMY_IMAGE_OPTIONS'];
        ?>
        <label for="width"><?php _e( 'Image Width', WPUT_TAX_NAME ); ?></label>
        <input type="text" id="width" name="<?php echo WPUT_TAX_OPTIONS; ?>[image_size][width]" value="<?php echo $options['image_size']['width']; ?>">px
        <br />
        <label for="height"><?php _e( 'Image Height', WPUT_TAX_NAME ); ?></label>
        <input type="text" id="height" name="<?php echo WPUT_TAX_OPTIONS; ?>[image_size][height]" value="<?php echo $options['image_size']['height']; ?>">px
        
        <p class="description"><?php _e( 'Image size in taxonomy/terms table', WPUT_TAX_NAME ); ?></p>
        <?php
    }

    /**
     * Sanitize options callback
     */
    public function sanitize_options( $options )
    {
        /**
         * For now just check image_size
         */
        if( isset( $options['image_size'] ) )
        {
            if( !ctype_digit( $options['image_size']['width'] ) ) $options['image_size']['width'] = '90';
            if( !ctype_digit( $options['image_size']['height'] ) ) $options['image_size']['height'] = '70';
        }

        return $options;
    }
}
endif;