<?php if ( ! defined( 'WPINC' ) ) { die( "Don't mess with us." ); }
/**
 * Hooks, actions and other helpers
 *
 * @since      1.0.0
 * @package    WP_Ultimate_Tools
 * @author     Goran Zivkovic
 */
if( !class_exists( 'WPUT_TAX_GLOBAL' ) && class_exists( 'WPUT_Global' ) ) :
class WPUT_TAX_GLOBAL extends WPUT_Global
{
    /**
     * All available options
     */
    public $deafult_options_tax_image = array(
        'taxonomies'    => array(),
        'image_size'    => array(
            'width'     => '90',
            'height'    => '70'
        )
    );

    /**
     * Class constructor
     */
    function __construct()
    {
        
    }
}
endif;