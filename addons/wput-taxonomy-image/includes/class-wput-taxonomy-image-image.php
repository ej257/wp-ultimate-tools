<?php if ( ! defined( 'WPINC' ) ) { die( "Don't mess with us." ); }
/**
 * Image system for taxonomies and categories
 *
 * @since      1.0.0
 * @package    WP_Ultimate_Tools
 * @author     Goran Zivkovic
 */
if( !class_exists( 'WPUT_TAX_IMAGE' ) ) :
class WPUT_TAX_IMAGE extends WPUT_TAX_GLOBAL
{
    /**
     * Class constructor
     */
    function __construct()
    {
        $options = $GLOBALS['WPUT_TAXONOMY_IMAGE_OPTIONS'];
        /**
         * Actions
         */
        if( !empty( $options['taxonomies'] ) )
        {
            foreach( $options['taxonomies'] as $tax )
            {
                $this->add_action( $tax . '_add_form_fields', 'add_taxonomy_image' );
                $this->add_action( $tax . '_edit_form_fields', 'edit_taxonomy_image' );
                $this->add_filter( 'manage_edit-' . $tax . '_columns', 'taxonomy_column_headers' );
                $this->add_filter( 'manage_'. $tax .'_custom_column', 'taxonomy_image_image', 10, 3 );
            }
        }
        $this->add_action( 'edit_term', 'term_image_save' );
        $this->add_action( 'create_term', 'term_image_save' );

        /**
         * Filters
         */
    }

    /**
     * Add taxonomy image
     */
    public function add_taxonomy_image( $tax )
    {
        ?>
        <form id="taxonomy-image">
            <div class="form-field" id="wput-taxonomy-image">
                <label for="taxonomy-image"><?php _e( 'Image', WPUT_TAX_NAME ); ?></label>
                <input type="text" name="taxonomy_image" class="image-url"><br />
                <button type="button" class="button button-primary" name="upload_image" id="image-button"><?php _e( 'Choose Image', WPUT_TAX_NAME ); ?></button>
                <button type="button" class="button button-secondary" name="remove_image" id="remove-image-button"><?php _e( 'Remove Image', WPUT_TAX_NAME ); ?></button>
                <p class="description"><?php _e( 'Click on the button to add/change taxonomy image', WPUT_TAX_NAME ); ?></p>
            </div>
        </form>
        <?php
        $this->enqueue_taxonomy_scripts();
    }

    /**
     * Edit taxonomy image
     */
    public function edit_taxonomy_image( $taxonomy )
    {
        ?>
        <tr class="form-field">
            <th scope="row" valign="top"><label for="taxonomy-image">Image</label></th>
                <td>
                <?php 
                    $this->display_image( $taxonomy->term_id );
                ?><br />
                <form id="taxonomy-image">
                    <div class="form-field" id="wput-taxonomy-image">
                        <label for="taxonomy-image"><?php _e( 'Image', WPUT_TAX_NAME ); ?></label>
                        <input type="text" name="taxonomy_image" class="image-url" value="<?php echo get_term_meta( $taxonomy->term_id, 'wput_term_image', true ); ?>"><br />
                        <button type="button" class="button button-primary" name="upload_image" id="image-button"><?php _e( 'Choose Image', WPUT_TAX_NAME ); ?></button>
                        <button type="button" class="button button-secondary" name="remove_image" id="remove-image-button"><?php _e( 'Remove Image', WPUT_TAX_NAME ); ?></button>
                        <p class="description"><?php _e( "Click on the button to add/change taxonomy image. Don't forget to update.", WPUT_TAX_NAME ); ?></p>
                    </div>
                </form>
                </td>
        </tr>
        <?php
        $this->enqueue_taxonomy_scripts();
    }

    /**
     * Enqueue required scripts
     */
    public function enqueue_taxonomy_scripts()
    {
        /**
         * Enqueue WP image uploader
         */
        wp_enqueue_media();

        /**
         * Taxonomy image JS
         */
        wp_register_script( WPUT_TAX_NAME, WPUT_TAX_ASSETS . '/js/wput-taxonomy-image.js', array( 'jquery' ), WPUT_TAX_VERSION, true);
        wp_enqueue_script( WPUT_TAX_NAME );
    } 

    /**
     * Update/Save term image 
     */
    public function term_image_save( $term_id ) 
    {
        if( isset( $_POST['taxonomy_image'] ) ) update_term_meta( $term_id, 'wput_term_image', $_POST['taxonomy_image'] );
        else delete_term_meta( $term_id, 'wput_term_image' );
    } 

    /**
     * Taxonomy column headers
     */
    public function taxonomy_column_headers( $columns )
    {
        $columns = array(
            'cb'                    => '<input type="checkbox" />',
            'wput_taxonomy_image'   => __( 'Image', WPUT_TAX_NAME ),
            'name'                  => __( 'Answer', WPUT_TAX_NAME ),
            'slug'                  => __( 'Slug', WPUT_TAX_NAME ),
            'description'           => __( 'Description', WPUT_TAX_NAME )
        );

        return $columns;
    }

    /**
     * Taxonomy column image
     */
    public function taxonomy_image_image( $content, $column_name, $term_id )
    {
        if( $column_name === 'wput_taxonomy_image' )
        {
            $this->display_image( $term_id );
        }
    }

    /**
     * Display image HTML
     */
    public function display_image( $term_id )
    {
        $options = $GLOBALS['WPUT_TAXONOMY_IMAGE_OPTIONS'];
        $image = get_term_meta( $term_id, 'wput_term_image', true );
        if( empty( $image ) )
        {
            ?>
            <img src="<?php echo WPUT_TAX_ASSETS . '/img/dummy-image.jpg'; ?>" class="wp-post-image" width="<?php echo $options['image_size']['width']; ?>"  height="<?php echo $options['image_size']['height']; ?>"/>
            <?php
        }
        else
        {
            ?>
            <img src="<?php echo esc_url( $image ); ?>" class="wp-post-image" width="<?php echo $options['image_size']['width']; ?>"  height="<?php echo $options['image_size']['height']; ?>"/>  
            <?php
        }
    }

}
endif;