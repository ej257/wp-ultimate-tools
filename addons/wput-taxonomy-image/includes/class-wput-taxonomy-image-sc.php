<?php if ( ! defined( 'WPINC' ) ) { die( "Don't mess with us." ); }
/**
 * Shortcodes
 *
 * @since      1.0.0
 * @package    WP_Ultimate_Tools
 * @author     Goran Zivkovic
 */

if( !class_exists( 'WPUT_TAX_SC' ) ) :
class WPUT_TAX_SC extends WPUT_TAX_GLOBAL
{
    /**
     * Class constructor
     */
    function __construct()
    {
        $this->add_shortcode( 'category_image', 'category_image' );
    }

    /**
     * Shortcode: [category_image]
     */
    public function category_image( $atts, $content = '' )
    {
        global $post;
        $options = $GLOBALS['WPUT_TAXONOMY_IMAGE_OPTIONS'];

        $post_taxonomies = get_object_taxonomies( $post, 'objects' );

        $image = '';
        foreach( $post_taxonomies as $name => $object )
        {
            if( $image !== '' ) break;
            if( !isset( $options['taxonomies'][ $name ] ) ) continue;

            $terms = get_terms( 
                array(
                    'taxonomy'      => $object->name,
                    'hide_empty'    => false
                )
            );

            if( empty( $terms ) || is_wp_error( $terms ) ) continue;

            foreach( $terms as $i => $term )
            {
                $img_tmp = get_term_meta( $term->term_id, 'image', true );
                if( !empty( $img_tmp ) )
                {
                    $image = $img_tmp;
                    break;
                }
            }
        }
        if( empty( $image ) ) return '';

        $defaults = array(
            'alt'       => $post->post_name,
            'title'     => $post->post_name,
            'class'     => 'wput-taxonomy-image',
            'style'     => 'width: 300px; height:250px;',
        );

        $atts = shortcode_atts(
            $defaults, 
            $atts, 
            'category_image'
        );

        if( strpos( $atts['class'], 'wput-taxonomy-image' ) === false ) $atts['class'] .= ' wput-taxonomy-image';
        
        return sprintf( '<img src="%s" class="%s" style="%s" title="%s" alt="%s">', $image, $atts['class'], $atts['style'], $atts['title'], $atts['alt'] );
    }
}
endif;