<?php if ( ! defined( 'WPINC' ) ) { die( "Don't mess with us." ); }
/**
 * Main Taxonomy Image Addon Class
 *
 * @since      1.0.0
 * @package    WP_Ultimate_Tools
 * @author     Goran Zivkovic
 */
if( !class_exists( 'WPUT_TAX_INIT' ) ) :
class WPUT_TAX_INIT extends WPUT_TAX_GLOBAL
{
    /**
     * Run all addon functions
     */
    public function run_image()
    {
        /**
         * Admin
         */
        if( file_exists( WPUT_TAX_INCLUDES . '/class-wput-taxonomy-image-admin.php' ) )
        {
            require_once WPUT_TAX_INCLUDES . '/class-wput-taxonomy-image-admin.php';
            if( class_exists( 'WPUT_TAX_ADMIN' ) ) new WPUT_TAX_ADMIN;
        }

        /**
         * Image system
         */
        if( file_exists( WPUT_TAX_INCLUDES . '/class-wput-taxonomy-image-image.php' ) )
        {
            require_once WPUT_TAX_INCLUDES . '/class-wput-taxonomy-image-image.php';
            if( class_exists( 'WPUT_TAX_IMAGE' ) ) new WPUT_TAX_IMAGE;
        }

        /**
         * Shortcodes
         */
        if( file_exists( WPUT_TAX_INCLUDES . '/class-wput-taxonomy-image-sc.php' ) )
        {
            require_once WPUT_TAX_INCLUDES . '/class-wput-taxonomy-image-sc.php';
            if( class_exists( 'WPUT_TAX_SC' ) ) new WPUT_TAX_SC;
        }
    }

    /**
     * Load addon text domain for translations
     */
    public function load_textdomain()
    {
        
    }
}
endif;