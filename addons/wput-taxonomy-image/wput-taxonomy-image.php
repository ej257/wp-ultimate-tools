<?php if ( ! defined( 'WPINC' ) ) { die( "Don't mess with us." ); }
/**
 * Addon for taxonomy and category image
 *
 * @since      1.0.0
 * @version    1.0.0
 * @package    WP_Ultimate_Tools
 * @author     Goran Zivkovic
 */

/**
 * Defines
 */
// Main plugin file
if ( ! defined( 'WPUT_TAX_FILE' ) )				define( 'WPUT_TAX_FILE', __FILE__ );
// Current plugin version
if ( ! defined( 'WPUT_TAX_VERSION' ) )			define( 'WPUT_TAX_VERSION', '1.0.0');
// Plugin root
if ( ! defined( 'WPUT_TAX_ROOT' ) )				define( 'WPUT_TAX_ROOT', rtrim(plugin_dir_path(WPUT_TAX_FILE), '/') );
// Includes directory
if ( ! defined( 'WPUT_TAX_INCLUDES' ) )			define( 'WPUT_TAX_INCLUDES', WPUT_TAX_ROOT . '/includes' );
// Includes directory
if ( ! defined( 'WPUT_TAX_ADMIN' ) )			define( 'WPUT_TAX_ADMIN', WPUT_TAX_ROOT . '/admin' );
// Plugin URL root
if ( ! defined( 'WPUT_TAX_URL' ) )				define( 'WPUT_TAX_URL', rtrim(plugin_dir_url( WPUT_TAX_FILE ), '/') );
// Assets URL
if ( ! defined( 'WPUT_TAX_ASSETS' ) )           define( 'WPUT_TAX_ASSETS', WPUT_TAX_URL.'/assets' );
// Plugin name
if ( ! defined( 'WPUT_TAX_NAME' ) )				define( 'WPUT_TAX_NAME', 'wput-taxonomy-image');
// Plugin session prefix (controlled by version)
if ( ! defined( 'WPUT_TAX_PREFIX' ) )			define( 'WPUT_TAX_PREFIX', 'wput_taxonomy_image_'.preg_replace("/[^0-9]/Ui",'',WPUT_TAX_VERSION).'_');
// Plugin prefix for hooks and others
if( ! defined( 'WPUT_TAX_HOOK' ) )              define( 'WPUT_TAX_HOOK', 'wput_taxonomy_image_' );
// Plugin options name
if( ! defined( 'WPUT_TAX_OPTIONS' ) )           define( 'WPUT_TAX_OPTIONS', 'wput_taxonomy_image_options' );

if( !class_exists( 'WPUT_TAX_GLOBAL' ) && !class_exists( 'WPUT_TAX_INIT' ) )
{
    /**
     * Include hook class
     */
    include_once WPUT_TAX_INCLUDES . '/class-wput-taxonomy-image-global.php';

    if( class_exists( 'WPUT_TAX_GLOBAL' ) )
    {
        /**
         * Call hook class and make all options global
         */
        $hook = new WPUT_TAX_GLOBAL;

        $options = $hook->get_option( WPUT_TAX_OPTIONS ); 
        if( empty( $options ) )
        {
            $hook->update_option( WPUT_TAX_OPTIONS, $hook->deafult_options_tax_image );
            $options = $hook->get_option( WPUT_TAX_OPTIONS );
        }
        $GLOBALS['WPUT_TAXONOMY_IMAGE_OPTIONS'] = $options;
        /**
         * Include main addon class
         */
        include_once WPUT_TAX_INCLUDES . '/class-wput-taxonomy-image.php';

        if( class_exists( 'WPUT_TAX_INIT' ) )
        {
            class WPUT_TAX_LOAD extends WPUT_TAX_INIT
            {
                function __construct()
                {
                    //$this->add_action( WPUT_HOOK . 'activate', 'activate_image' );
                    //$this->add_action( WPUT_HOOK . 'deactivate', 'deactivate_image' );
                    do_action( WPUT_TAX_HOOK . 'init' );
                    $this->run_image();
                }
            }
        }
        /**
         * Clear CPU and RAM
         */
        $hook = NULL;
    }

    /**
     * Everything is probably loaded
     */
    function wput_taxonomy_image()
    {
        if( class_exists( 'WPUT_TAX_LOAD' ) ) return new WPUT_TAX_LOAD;
    }

    /**
     * Let's start the show
     */
    wput_taxonomy_image();
}