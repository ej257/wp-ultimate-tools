<?php if ( ! defined( 'WPINC' ) ) { die( "Don't mess with us." ); }
/**
 * Hooks, actions and other helpers
 *
 * @since      1.0.0
 * @package    WP_Ultimate_Tools
 * @author     Goran Zivkovic
 */
if( !class_exists( 'WPUT_W3C_GLOBAL' ) && class_exists( 'WPUT_Global' ) ) :
class WPUT_W3C_GLOBAL extends WPUT_Global
{
    /**
     * Class constructor
     */
    function __construct()
    {
        
    }
}
endif;