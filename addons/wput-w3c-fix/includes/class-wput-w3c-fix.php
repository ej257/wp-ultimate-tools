<?php if ( ! defined( 'WPINC' ) ) { die( "Don't mess with us." ); }
/**
 * Main W3C Fix Addon Class
 *
 * @since      1.0.0
 * @package    WP_Ultimate_Tools
 * @author     Goran Zivkovic
 */
if( !class_exists( 'WPUT_W3C_INIT' ) ) :
class WPUT_W3C_INIT extends WPUT_W3C_GLOBAL
{
    /**
     * Run all addon functions
     */
    public function run_w3c()
    {
        /**
         * Fix system
         */
        if( file_exists( WPUT_W3C_INCLUDES . '/class-wput-w3c-fix-fix.php' ) )
        {
            require_once WPUT_W3C_INCLUDES . '/class-wput-w3c-fix-fix.php';
            if( class_exists( 'WPUT_W3C_FIX' ) ) new WPUT_W3C_FIX;
        }
    }

    /**
     * Load addon text domain for translations
     */
    public function load_textdomain()
    {
        
    }
}
endif;