<?php if ( ! defined( 'WPINC' ) ) { die( "Don't mess with us." ); }
/**
 * Addon for W3C fix
 *
 * @since      1.0.0
 * @version    1.0.0
 * @package    WP_Ultimate_Tools
 * @author     Goran Zivkovic
 */

/**
 * Defines
 */
// Main plugin file
if ( ! defined( 'WPUT_W3C_FILE' ) )				define( 'WPUT_W3C_FILE', __FILE__ );
// Current plugin version
if ( ! defined( 'WPUT_W3C_VERSION' ) )			define( 'WPUT_W3C_VERSION', '1.0.0');
// Plugin root
if ( ! defined( 'WPUT_W3C_ROOT' ) )				define( 'WPUT_W3C_ROOT', rtrim(plugin_dir_path(WPUT_W3C_FILE), '/') );
// Includes directory
if ( ! defined( 'WPUT_W3C_INCLUDES' ) )			define( 'WPUT_W3C_INCLUDES', WPUT_W3C_ROOT . '/includes' );
// Includes directory
if ( ! defined( 'WPUT_W3C_ADMIN' ) )			define( 'WPUT_W3C_ADMIN', WPUT_W3C_ROOT . '/admin' );
// Plugin URL root
if ( ! defined( 'WPUT_W3C_URL' ) )				define( 'WPUT_W3C_URL', rtrim(plugin_dir_url( WPUT_W3C_FILE ), '/') );
// Assets URL
if ( ! defined( 'WPUT_W3C_ASSETS' ) )           define( 'WPUT_W3C_ASSETS', WPUT_W3C_URL.'/assets' );
// Plugin name
if ( ! defined( 'WPUT_W3C_NAME' ) )				define( 'WPUT_W3C_NAME', 'wput-w3c-fix');
// Plugin session prefix (controlled by version)
if ( ! defined( 'WPUT_W3C_PREFIX' ) )			define( 'WPUT_W3C_PREFIX', 'wput_w3c_fix_'.preg_replace("/[^0-9]/Ui",'',WPUT_W3C_VERSION).'_');
// Plugin prefix for hooks and others
if( ! defined( 'WPUT_W3C_HOOK' ) )              define( 'WPUT_W3C_HOOK', 'wput_w3c_fix_' );
// Plugin options name
if( ! defined( 'WPUT_W3C_OPTIONS' ) )           define( 'WPUT_W3C_OPTIONS', 'wput_w3c_fix_options' );

if( !class_exists( 'WPUT_W3C_GLOBAL' ) && !class_exists( 'WPUT_W3C_INIT' ) )
{
    /**
     * Include hook class
     */
    include_once WPUT_W3C_INCLUDES . '/class-wput-w3c-fix-global.php';

    if( class_exists( 'WPUT_W3C_GLOBAL' ) )
    {
        /**
         * Call hook class and make all options global
         */
        $hook = new WPUT_W3C_GLOBAL;

        /*$options = $hook->get_option( WPUT_W3C_OPTIONS ); 
        if( empty( $options ) )
        {
            $hook->update_option( WPUT_W3C_OPTIONS, $hook->deafult_options_tax_image );
            $options = $hook->get_option( WPUT_W3C_OPTIONS );
        }
        $GLOBALS['WPUT_W3CONOMY_IMAGE_OPTIONS'] = $options;
        /**
         * Include main addon class
         */
        include_once WPUT_W3C_INCLUDES . '/class-wput-w3c-fix.php';

        if( class_exists( 'WPUT_W3C_INIT' ) )
        {
            class WPUT_W3C_LOAD extends WPUT_W3C_INIT
            {
                function __construct()
                {
                    //$this->add_action( WPUT_HOOK . 'activate', 'activate_image' );
                    //$this->add_action( WPUT_HOOK . 'deactivate', 'deactivate_image' );
                    do_action( WPUT_W3C_HOOK . 'init' );
                    $this->run_w3c();
                }
            }
        }
        /**
         * Clear CPU and RAM
         */
        $hook = NULL;
    }

    /**
     * Everything is probably loaded
     */
    function wput_w3c_fix()
    {
        if( class_exists( 'WPUT_W3C_LOAD' ) ) return new WPUT_W3C_LOAD;
    }

    /**
     * Let's start the show
     */
    wput_w3c_fix();
}