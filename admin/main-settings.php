<?php if ( ! defined( 'WPINC' ) ) { die( "Don't mess with us." ); }
/**
 * Settings Page
 *
 * @since      1.0.0
 * @package    WP_Ultimate_Tools
 * @author     Goran Zivkovic
 */
$wput_settings = '';
if( !isset( $_GET['tab'] ) ) $wput_settings = ' nav-tab-active';
elseif( $_GET['tab'] == 'wput_settings' ) $wput_settings = ' nav-tab-active';
?>
<div class="wrap">
	<h2><?php esc_attr_e( 'Tabs', WPUT_NAME ); ?></h2>
	<hr>
	<h2 class="nav-tab-wrapper">
		<a href="<?php echo self_admin_url( 'admin.php?page=' . $_GET['page'] . '&tab=wput_settings' ); ?>" class="nav-tab <?php echo $wput_settings ?>"><?php _e( 'WP Ultimate Tools', WPUT_NAME ); ?></a>
		<?php do_action( WPUT_HOOK . 'settings_tabs' ); ?>
	</h2>
	<div class="poststuff">
	<?php 
		if( $wput_settings == ' nav-tab-active' )
		{
			?>
			<div id="post-body" class="metabox-holder columns-1">
				<div id="postbox-container" class="postbox-container-1">
					<form method="post" action="options.php">
						<div class="meta-box-sortables ui-sortable" id="normal-sortables">
							<div class="postbox " id="itsec_get_started">
								<h3 class="hndle"><span><?php _e( 'WP Ultimate Tools General Settings', WPUT_NAME ); ?></span></h3>
								<div class="inside">
									<?php 
										settings_fields( WPUT_NAME . '-settings' );

										do_settings_sections( WPUT_NAME . '-page-settings' );

										submit_button();
									?>
									<div class="clear"></div>
								</div>
							</div>
						</div>
					</form>
				</div>
			</div>
			<?php
		}
		do_action( WPUT_HOOK . 'settings' ); 
	?>
	</div>
</div>