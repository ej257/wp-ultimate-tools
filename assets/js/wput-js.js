(function($){
    /**
     * Select/Deselect all options
     */
    $( 'input#select-all' ).on( 'input click check change', function() {
        var checkbox = $( this );

        if( checkbox.is( ':checked' ) ) 
        {
            checkboxes = checkbox.closest( 'td' ).find( ':checkbox' );
            checkboxes.prop( 'checked', true );
        }
        else 
        {
            checkboxes = checkbox.closest( 'td' ).find( ':checkbox' );
            checkboxes.prop( 'checked', false );
        }
        
    });
})(jQuery || window.jQuery || Zepto || window.Zepto);