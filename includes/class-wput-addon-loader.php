<?php if ( ! defined( 'WPINC' ) ) { die( "Don't mess with us." ); }
/**
 * Auto-Loading addon plugins
 *
 * @since      1.0.0
 * @package    WP_Ultimate_Tools
 * @author     Goran Zivkovic
 * 
 * This class allows us to auto-load any new or old addon in addons directory
 * RULE: Main addon file, function that calls class, should have same basename as parent directory and .php extension. 
 * ( in example below ). 
 * ============ MAIN FILES ============
 * Good File Examples:
 * /addons
 *      /addon-name/addon-name.php
 * 
 * /addons
 *      /Addon-Name/Addon-Name.php
 * -----------------------------------
 * Bad File Example:
 * /addons
 *      /addon-name/addonname1.php
 * 
 * 
 * For more info check main plugin files at addons directory
 * 
 * ============ IMPORTANT ============
 * Please be aware that loader is and must be case sensitive. 
 */
if( !class_exists( 'WPUT_Addons_Loader' ) ) :
class WPUT_Addons_Loader
{
    /**
     * Array of all collected paths
     */
    private $addons_paths;

    /**
     * Class constructor
     */
    function __construct()
    {
        /**
         * Initialize class properties
         */
        $this->addons_paths = array();

        /**
         * Call method to collect all paths and load all classes
         */
        $this->get_files( WPUT_ADDONS );
        $this->addons_load();
    }

    /**
     * Method that iterates in subdirectories
     */
    public function get_files( $path ) 
    {
        if( empty( $path ) ) $path = WPUT_ADDONS;
        else $path = str_replace( '\\', '/', $path );
        
        if( !file_exists( $path ) ) return false; // Stop proccess on time if directory does not exists

        /**
         * First check if class SplFileInfo exists
         * 
         * SplFileInfo > DirectoryIterator > FilesystemIterator
         * 
         * SplFileInfo - Exists in PHP 5.1.2+
         * DirectoryIterator - Exists in PHP 5.1.2+
         * FilesystemIterator - Exists in PHP 5.3.0+
         */
        if( class_exists( 'SplFileInfo' ) ) 
        {
            if( class_exists( 'FilesystemIterator' ) )
            {
                $it = new FilesystemIterator( $path );
                $v = 1;
            }
            elseif( class_exists( 'DirectoryIterator' ) )
            {
                $it = new DirectoryIterator( $path );
                $v = 2;
            }
            foreach( $it as $fileinfo )
            {
                if( $v === 2 && $fileinfo->isDot() ) continue; // FilesystemIterator automatically avoids virtual and hidden files
                if( $fileinfo->isDir() ) // Collect only directories
                {
                    $this->addons_paths[ $fileinfo->getBasename() ] = str_replace( '\\', '/', $fileinfo->getPathname() ); 
                }

                $GLOBALS['WPUT_ADDONS_PATHS'] = $this->addons_paths; // I can't find better solution
            }
        }
        else return false; // Only possible solution would be function glob() but glob only searches file in directory where script is; maybe scandir?
    }

    /**
     * Method that loads all addons
     */
    public function addons_load()
    {
        do_action( 'wput_addons_load', $this->addons_paths );
        if( empty( $this->addons_paths ) ) return false;

        $options_main = $GLOBALS['WPUT_OPTIONS'];

        apply_filters( 'wput_addons_paths', $this->addons_paths );

        foreach( $this->addons_paths as $dir_name => $dir_path )
        {
            $dir_path = rtrim( $dir_path, '/' );
            $file_basename = explode( '/', $dir_path );
            $file_basename = end( $file_basename );
            $file_basename .= '.php';
            
            $file_url = str_replace( '\\', '/', $dir_path . '/' . $file_basename ); // Just in case

            if( file_exists( $file_url ) && isset( $options_main['addons'][$dir_path] ) )
            {
                include_once $file_url;
            }
        }
    }

}
endif;