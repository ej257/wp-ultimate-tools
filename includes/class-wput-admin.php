<?php if ( ! defined( 'WPINC' ) ) { die( "Don't mess with us." ); }
/**
 * Admin side of plugin
 *
 * @since      1.0.0
 * @package    WP_Ultimate_Tools
 * @author     Goran Zivkovic
 */
if( !class_exists( 'WPUT_Admin' ) ) :
class WPUT_Admin extends WPUT_Global
{
    /**
     * Class constructor
     */
    function __construct()
    {
        /**
         * Actions
         */
        $this->add_action( 'admin_menu', 'add_wput_menu' );   
        $this->add_action( 'init', 'load_styles' );
        $this->add_action( 'init', 'load_scripts' );
        $this->add_action( 'admin_init', 'settings_init' );

        /**
         * Filters
         */

        do_action( 'wput_admin_construct' );
    }

    /**
     * Add WPUT admin menu
     */
    public function add_wput_menu()
    {
        /**
         * Menu: WP Ultimate Tools
         * 
         * add_menu_page($page_title, $menu_title, $capability, $menu_slug, $function, $icon_url, $position)
         */
        add_menu_page(
            __( 'WP Ultimate Tools', WPUT_NAME ),
            __( 'WP Ultimate Tools', WPUT_NAME ),
            'manage_options',
            WPUT_NAME,
            array( &$this, 'page_wput' ),
            'dashicons-hammer'
        );

        /**
         * Submenu: Settings
         *
         * add_submenu_page($parent_slug, $page_title, $menu_title, $capability, $menu_slug, $function);
         */
        add_submenu_page(
            WPUT_NAME,
            __( 'Settings', WPUT_NAME ),
            __( 'Settings', WPUT_NAME ),
            'manage_options',
            WPUT_NAME . '-page-settings',
            array( &$this, 'page_settings' )
        );
 


        do_action( 'wput_admin_menu' );
    }

    /**
     * Load all styles
     */
    public function load_styles()
    {
        $this->add_action( 'admin_enqueue_scripts', 'register_style' );
    }

    /**
     * Load all scripts
     */
    public function load_scripts()
    {
        $this->add_action( 'admin_enqueue_scripts', 'register_scripts' );
    }

    /**
     * Main plugin style
     */
    public function register_style()
    {
        return false;
    }

    /**
     * Main plugin scripts
     */
    public function register_scripts( $page )
    {
        if( !$this->limit_scripts( $page ) ) return false;

        wp_register_script( WPUT_NAME . '-js', WPUT_ASSETS . '/js/wput-js.js', array( 'jquery' ), WPUT_VERSION, true );
        wp_enqueue_script( WPUT_NAME . '-js' );
    }

    /**
     * Main menu page
     */
    public function page_wput()
    {
        if( file_exists( WPUT_ADMIN . '/wput.php' ) ) require_once WPUT_ADMIN . '/wput.php';
    }

    /**
     * Settings page
     */
    public function page_settings()
    {
        if( file_exists( WPUT_ADMIN . '/main-settings.php' ) ) require_once WPUT_ADMIN . '/main-settings.php';
    }

    /**
     * WPUT settings and options
     */
    public function settings_init()
    {
        /**
         * Register settings
         * 
         * register_setting($option_group, $option_name, $sanitize_callback);
         */
        register_setting(
            WPUT_NAME . '-settings',
            WPUT_OPTIONS,
            array(
                'default'   => $this->default_options_wput
            )
        );

        /**
         * Add general section
         * 
         * add_settings_section($id, $title, $callback, $page);
         */
        add_settings_section(
            WPUT_NAME . '-general-options',
            apply_filters( WPUT_HOOK . 'general_settings_title', __( 'General Options', WPUT_NAME ) ),
            array( &$this, 'wput_general_section' ),
            WPUT_NAME . '-page-settings'
        );

        /**
         * Add settings field to section
         * 
         * add_settings_field($id, $title, $callback, $page, $section, $args);
         */
        add_settings_field(
            'addons',
            apply_filters( WPUT_HOOK . 'addons_title', __( 'Available addons', WPUT_NAME ) ),
            array( &$this, 'addons' ),
            WPUT_NAME . '-page-settings',
            WPUT_NAME . '-general-options'
        );
    }

    /**
     * Section callback
     */
    public function wput_general_section()
    {
        return false;
    }

    /**
     * Addons options callback
     */
    public function addons()
    {
        $options = $GLOBALS['WPUT_OPTIONS'];
        $addons_paths = $GLOBALS['WPUT_ADDONS_PATHS'];
        
        if( !empty( $addons_paths ) )
        {
            ?>
            <input type="checkbox" name="select_all" id="select-all"><?php _e( 'Select All', WPUT_NAME ); ?><br /><br />
            <?php
            foreach( $addons_paths as $dir_name => $dir_path )
            {
                $dir_path = rtrim( $dir_path, '/' );
                $file_basename = explode( '/', $dir_path );
                $file_basename = end( $file_basename );
                
                $file_basename = explode( '-', $file_basename );
                $basename = '';
                foreach( $file_basename as $i => $word )
                {
                    if( strtolower( $word ) ===  'wput' ) $word = strtoupper( $word );
                    $basename .= ucfirst( $word ) . ' ';
                }
                $checked = '';
                if( isset( $options['addons'][$dir_path] ) ) $checked = ' checked';
                ?>
                <input type="checkbox" name="<?php echo WPUT_OPTIONS; ?>[addons][<?php echo $dir_path; ?>]" value="<?php echo $dir_path ?>" <?php echo $checked; ?>><?php echo $basename; ?><br />
                <?php
            }
            ?>
            <p class="description"><?php _e( 'Select addons that you want to use', WPUT_NAME ); ?></p>
            <?php
        }
    }

    /**
     * Limit scripts
     */
    public function limit_scripts( $page )
    {
        if( strpos( $page, WPUT_NAME ) !== false ) return true;

        return false;
    }
}
endif;