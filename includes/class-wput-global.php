<?php if ( ! defined( 'WPINC' ) ) { die( "Don't mess with us." ); }
/**
 * SUPER-GLOBAL class for hook, helper functions, etc...
 *
 * @since      1.0.0
 * @package    WP_Ultimate_Tools
 * @author     Goran Zivkovic
 */
if( !class_exists( 'WPUT_Global' ) ) :
class WPUT_Global
{
    /**
     * All options
     */
    public $default_options_wput = array(
        'addons'    => array()
    );

    /**
     * Class constructor
     */
    function __construct()
    {
        
    }
    
    /**
     * Hook for register_activation_hook()
     */
    public function register_activation_hook( $file, $function )
    {
        if( !is_array( $function ) ) $function = array( &$this, $function );
        
        return register_activation_hook( $file, $function );
    }

    /**
     *  Hook for register_deactivation_hook()
     */
    public function register_deactivation_hook( $file, $function )
    {
        if( !is_array( $function ) ) $function = array( &$this, $function );
        
        return register_deactivation_hook( $file, $function );
    }

    /** 
     * Hook for add_action()
    */
    public function add_action( $hook, $function, $priority = 10, $accepted_args = 1 )
    {
        if( !is_array( $function ) ) $function = array( &$this, $function );

        return add_action( (string)$hook, $function, (int)$priority, (int)$accepted_args );
    }

    /**
     * Hook for add_filter()
     */
    public function add_filter( $hook, $function, $priority = 10, $accepted_args = 1 )
    {
        if( !is_array( $function ) ) $function = array( &$this, $function );

        return add_filter( (string)$hook, $function, (int)$priority, (int)$accepted_args );
    }

    /**
     * Hook for remove_filter()
     */
    public function remove_filter( $hook, $function, $priority = 10 )
    {
        if( !is_array( $function ) ) $function = array( &$this, $function );

        return remove_filter( (string)$hook, $function, (int)$priority );
    }

    /**
     * Hook for remove_action()
     */
    public function remove_action( $hook, $function, $priority = 10 )
    {
        if( !is_array( $function ) ) $function = array( &$this, $function );
   
        return remove_action( (string)$hook, $function, (int)$priority );
	}
	
	/**
	 * Hook for add_shortcode()
	 */
	public function add_shortcode( $hook, $function )
	{
		if( !is_array( $function ) ) $function = array( &$this, $function );

		return add_shortcode( $hook, $function );
	}

    /**
	 * Get real URL
	 */
	public static function URL(){
		$http = 'http'.((isset($_SERVER['HTTPS']) && !empty($_SERVER['HTTPS']) && $_SERVER['HTTPS']!='off' || $_SERVER['SERVER_PORT']==443)?'s':'');
		$domain = str_replace(array("\\","//",":/"),array("/","/",":///"),$http.'://'.$_SERVER['HTTP_HOST']);
		$url = str_replace(array("\\","//",":/"),array("/","/",":///"),$domain.'/'.$_SERVER['REQUEST_URI']);
			
		return (object) array(
			"method"	=>	$http,
			"home_fold"	=>	str_replace($domain,'',home_url()),
			"url"		=>	$url,
			"domain"	=>	$domain,
			"hostname"	=>	self::get_host(),
		);
    }
    
    /**
     * Check active settings page and tabs
     */
    public function is_settings_active( $tab )
    {
        if( isset( $_GET['tab'] ) && $_GET['tab'] === $tab ) return true;
        return false;
    }

    /**
     * Hook for get_option()
     */
    public function get_option( $option_name )
    {
        if( WPUT_MULTISITE ) $options = get_site_option( $option_name );
        else $options = get_option( $option_name );

        return $options;
    }

    /**
     * Hook for update_option()
     */
    public function update_option( $option_name, $value )
    {
        if( WPUT_MULTISITE ) update_site_option( $option_name, $value );
        else update_option( $option_name, $value, true );
    }

    /**
     * Convert URL to post ID 
     */
	public function get_url_to_postid($url)
	{
		global $wp_rewrite;

		$url = apply_filters('url_to_postid', $url);

		// First, check to see if there is a 'p=N' or 'page_id=N' to match against
		if ( preg_match('#[?&](p|page_id|attachment_id)=(\d+)#', $url, $values) )	{
			$id = absint($values[2]);
			if ( $id )
				return $id;
		}

		// Check to see if we are using rewrite rules
		$rewrite = $wp_rewrite->wp_rewrite_rules();

		// Not using rewrite rules, and 'p=N' and 'page_id=N' methods failed, so we're out of options
		if ( empty($rewrite) )
			return 0;

		// Get rid of the #anchor
		$url_split = explode('#', $url);
		$url = $url_split[0];

		// Get rid of URL ?query=string
		$url_split = explode('?', $url);
		$url = $url_split[0];

		// Add 'www.' if it is absent and should be there
		if ( false !== strpos(home_url(), '://www.') && false === strpos($url, '://www.') )
			$url = str_replace('://', '://www.', $url);

		// Strip 'www.' if it is present and shouldn't be
		if ( false === strpos(home_url(), '://www.') )
			$url = str_replace('://www.', '://', $url);

		// Strip 'index.php/' if we're not using path info permalinks
		if ( !$wp_rewrite->using_index_permalinks() )
			$url = str_replace('index.php/', '', $url);

		if ( false !== strpos($url, home_url()) ) {
			// Chop off http://domain.com
			$url = str_replace(home_url(), '', $url);
		} else {
			// Chop off /path/to/blog
			$home_path = parse_url(home_url());
			$home_path = isset( $home_path['path'] ) ? $home_path['path'] : '' ;
			$url = str_replace($home_path, '', $url);
		}

		// Trim leading and lagging slashes
		$url = trim($url, '/');

		$request = $url;
		// Look for matches.
		$request_match = $request;
		foreach ( (array)$rewrite as $match => $query) {
			// If the requesting file is the anchor of the match, prepend it
			// to the path info.
			if ( !empty($url) && ($url != $request) && (strpos($match, $url) === 0) )
				$request_match = $url . '/' . $request;

			if ( preg_match("!^$match!", $request_match, $matches) ) {
				// Got a match.
				// Trim the query of everything up to the '?'.
				$query = preg_replace("!^.+\?!", '', $query);

				// Substitute the substring matches into the query.
				$query = addslashes(WP_MatchesMapRegex::apply($query, $matches));

				// Filter out non-public query vars
				global $wp;
				parse_str($query, $query_vars);
				$query = array();
				foreach ( (array) $query_vars as $key => $value ) {
					if ( in_array($key, $wp->public_query_vars) )
						$query[$key] = $value;
				}

			// Taken from class-wp.php
			foreach ( $GLOBALS['wp_post_types'] as $post_type => $t )
				if ( $t->query_var )
					$post_type_query_vars[$t->query_var] = $post_type;

			foreach ( $wp->public_query_vars as $wpvar ) {
				if ( isset( $wp->extra_query_vars[$wpvar] ) )
					$query[$wpvar] = $wp->extra_query_vars[$wpvar];
				elseif ( isset( $_POST[$wpvar] ) )
					$query[$wpvar] = $_POST[$wpvar];
				elseif ( isset( $_GET[$wpvar] ) )
					$query[$wpvar] = $_GET[$wpvar];
				elseif ( isset( $query_vars[$wpvar] ) )
					$query[$wpvar] = $query_vars[$wpvar];

				if ( !empty( $query[$wpvar] ) ) {
					if ( ! is_array( $query[$wpvar] ) ) {
						$query[$wpvar] = (string) $query[$wpvar];
					} else {
						foreach ( $query[$wpvar] as $vkey => $v ) {
							if ( !is_object( $v ) ) {
								$query[$wpvar][$vkey] = (string) $v;
							}
						}
					}

					if ( isset($post_type_query_vars[$wpvar] ) ) {
						$query['post_type'] = $post_type_query_vars[$wpvar];
						$query['name'] = $query[$wpvar];
					}
				}
			}

				// Do the query
				$query = new WP_Query($query);
				if ( !empty($query->posts) && $query->is_singular )
					return $query->post->ID;
				else
					return 0;
			}
		}
		return 0;
	}

	/* 
	* Generate and clean POST
	* @name          POST name
	* @option        string, int, float, bool, html, encoded, url, email
	* @default       default value
	*/
	public function post($name, $option="string", $default='')
	{
		$option = trim((string)$option);
		if(isset($_POST[$name]) && !empty($_POST[$name]))
		{        
			if(is_array($_POST[$name]))
				$is_array=true;
			else
				$is_array=false;
			
			if( is_numeric( $option ) || empty( $option ) ) return $default;
			else $input = $_POST[$name];
			
			switch($option)
			{
				default:
				case 'string':
				case 'html':
					if($is_array) return array_map( 'sanitize_text_field', $input );
					
					return sanitize_text_field( $input );
				break;
				case 'encoded':
					return (!empty($input)?$input:$default);
				break;
				case 'url':
					if($is_array) return array_map( 'esc_url', $input );
			
					return esc_url( $input );
				break;
				case 'url_raw':
					if($is_array) return array_map( 'esc_url_raw', $input );
		
					return esc_url_raw( $input );
				break;
				case 'email':
					if($is_array) return array_map( 'sanitize_email', $input );
					
					return sanitize_email( $input );
				break;
				case 'int':
					if($is_array) return array_map( 'absint', $input );
					
					return absint( $input );
				break;
				case 'float':
					if($is_array) return array_map( 'floatval', $intput );
					
					return floatval( $input );
				break;
				case 'bool':
					if($is_array) return array_map( 'boolval', $input );
					
					return boolval( $input );
				break;
				case 'html_class':
					if( $is_array ) return array_map( 'sanitize_html_class', $input );

					return sanitize_html_class( $input );
				break;
				case 'title':
					if( $is_array ) return array_map( 'sanitize_title', $input );

					return sanitize_title( $input );
				break;
				case 'user':
					if( $is_array ) return array_map( 'sanitize_user', $input );

					return sanitize_user( $input );
				break;
				case 'no_html':
					if( $is_array ) return array_map( 'wp_filter_nohtml_kses', $input );

					return wp_filter_nohtml_kses( $input );
				break;
				case 'post':
					if( $is_array ) return array_map( 'wp_filter_post_kses', $input );

					return wp_filter_post_kses( $input );
				break;
			}
		}
		else
		{
			return $default;
		}
	}

	
	/* 
	* Generate and clean GET
	* @name          GET name
	* @option        string, int, float, bool, html, encoded, url, email
	* @default       default value
	*/
	public function get($name, $option="string", $default='')
	{
        $option = trim((string)$option);
        if(isset($_GET[$name]) && !empty($_GET[$name]))
        {           
            if(is_array($_GET[$name]))
                $is_array=true;
            else
                $is_array=false;
            
            if( is_numeric( $option ) || empty( $option ) ) return $default;
            else $input = $_GET[$name];
            
            switch($option)
            {
                default:
                case 'string':
                case 'html':
                    if($is_array) return array_map( 'sanitize_text_field', $input );
                    
                    return sanitize_text_field( $input );
                break;
                case 'encoded':
                    return (!empty($input)?$input:$default);
                break;
				case 'url':
					if($is_array) return array_map( 'esc_url', $input );
			
					return esc_url( $input );
				break;
				case 'url_raw':
					if($is_array) return array_map( 'esc_url_raw', $input );
		
					return esc_url_raw( $input );
				break;
                case 'email':
                    if($is_array) return array_map( 'sanitize_email', $input );
                    
                    return sanitize_email( $input );
                break;
                case 'int':
                    if($is_array) return array_map( 'absint', $input );
                    
                    return absint( $input );
                break;
                case 'float':
					if($is_array) return array_map( 'floatval', $intput );
                    
                    return floatval( $input );
                break;
                case 'bool':
                    if($is_array) return array_map( 'boolval', $input );
                    
                    return boolval( $input );
				break;
				case 'html_class':
					if( $is_array ) return array_map( 'sanitize_html_class', $input );

					return sanitize_html_class( $input );
				break;
				case 'title':
					if( $is_array ) return array_map( 'sanitize_title', $input );

					return sanitize_title( $input );
				break;
				case 'user':
					if( $is_array ) return array_map( 'sanitize_user', $input );

					return sanitize_user( $input );
				break;
				case 'no_html':
					if( $is_array ) return array_map( 'wp_filter_nohtml_kses', $input );

					return wp_filter_nohtml_kses( $input );
				break;
				case 'post':
					if( $is_array ) return array_map( 'wp_filter_post_kses', $input );

					return wp_filter_post_kses( $input );
				break;
            }
        }
        else
        {
            return $default;
        }
    }
}
endif;