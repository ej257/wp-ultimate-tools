<?php if ( ! defined( 'WPINC' ) ) { die( "Don't mess with us." ); }
/**
 * Main Plugin Class
 *
 * @since      1.0.0
 * @package    WP_Ultimate_Tools
 * @author     Goran Zivkovic
 */
if( !class_exists( 'WPUT_Init' ) ) :
class WPUT_Init extends WPUT_Global
{
    /**
     * Run all plugin functions 
     */
    public function run()
    {
        /**
         * Admin class
         */
        if( file_exists( WPUT_INCLUDES . '/class-wput-admin.php' ) )
        {
            require_once WPUT_INCLUDES . '/class-wput-admin.php';
            if( class_exists( 'WPUT_Admin' ) )
            {
                new WPUT_Admin;
            }
        }

        do_action( WPUT_HOOK . 'class_load' );
    }

    /**
     * Activation Hook Callback
     */
    public function activate()
    {
        do_action( WPUT_HOOK . 'activate' );
    }

    /**
     * Deactivation Hook Callback
     */
    public function deactivate()
    {
        do_action( WPUT_HOOK . 'deactivate' );
    }

    /**
     * Load plugin text domain for translations
     */
    public function load_textdomain()
    {
        
    }
}
endif;