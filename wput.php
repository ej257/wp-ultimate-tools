<?php
/**
 * @link              //
 * @since             1.0.0
 * @package           WP_Ultimate_Tools
 *
 * @wordpress-plugin
 * Plugin Name:       WP Ultimate Tools
 * Plugin URI:        //
 * Description:       //
 * Version:           1.0.0
 * Author:            Ivijan-Stefan Stipic | Goran Zivkovic
 * Author URI:        https://linkedin.com/in/ivijanstefanstipic
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       wp-ultimate-tools
 * Domain Path:       /languages
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 */
 
// If someone try to called this file directly via URL, abort.
if ( ! defined( 'WPINC' ) ) { die( "Don't mess with us." ); }
if ( ! defined( 'ABSPATH' ) ) { exit; }

/**
 * Defines
 */
// Main plugin file
if ( ! defined( 'WPUT_FILE' ) )				define( 'WPUT_FILE', __FILE__ );
// Current plugin version
if ( ! defined( 'WPUT_VERSION' ) )			define( 'WPUT_VERSION', '1.0.0');
// Plugin root
if ( ! defined( 'WPUT_ROOT' ) )				define( 'WPUT_ROOT', rtrim(plugin_dir_path(WPUT_FILE), '/') );
// Includes directory
if ( ! defined( 'WPUT_INCLUDES' ) )			define( 'WPUT_INCLUDES', WPUT_ROOT . '/includes' );
// Includes directory
if ( ! defined( 'WPUT_ADMIN' ) )			define( 'WPUT_ADMIN', WPUT_ROOT . '/admin' );
// Plugin URL root
if ( ! defined( 'WPUT_URL' ) )				define( 'WPUT_URL', rtrim(plugin_dir_url( WPUT_FILE ), '/') );
// Assets URL
if ( ! defined( 'WPUT_ASSETS' ) )			define( 'WPUT_ASSETS', WPUT_URL.'/assets' );
// Plugin name
if ( ! defined( 'WPUT_NAME' ) )				define( 'WPUT_NAME', 'wp-ultimate-tools');
// Plugin session prefix (controlled by version)
if ( ! defined( 'WPUT_PREFIX' ) )			define( 'WPUT_PREFIX', 'wp_ut_'.preg_replace("/[^0-9]/Ui",'',WPUT_VERSION).'_');
// Plugin addons directory
if( ! defined( 'WPUT_ADDONS' ) )            define( 'WPUT_ADDONS', WPUT_ROOT . '/addons' );
// Plugin custom hooks prefix
if( ! defined( 'WPUT_HOOK' ) )              define( 'WPUT_HOOK', 'wput_' );
// Plugin options
if( ! defined( 'WPUT_OPTIONS' ) )           define( 'WPUT_OPTIONS', 'wput_options' );
// Check if is multisite installation
if( ! defined( 'WPUT_MULTISITE' ) )			
{
    // New safer approach
    if( !function_exists( 'is_plugin_active_for_network' ) ) require_once( ABSPATH . '/wp-admin/includes/plugin.php' );

    if( is_plugin_active_for_network( WPUT_ROOT . '/wput.php' ) ) define( 'WPUT_MULTISITE', true );
    else define( 'WPUT_MULTISITE', false );
}

if( !class_exists( 'WPUT_Global' ) && !class_exists( 'WPUT_Init' ) )
{
    /**
     * Include hook classes
     */
    include_once WPUT_INCLUDES . '/class-wput-global.php';


    if( class_exists( 'WPUT_Global' ) )
    {
        /**
         * Call hook class and make options global
         */
        $main_hook = new WPUT_Global;
        
        $options_main = $main_hook->get_option( WPUT_OPTIONS );
        if( empty( $options_main ) )
        {
            $main_hook->update_option( WPUT_OPTIONS, $main_hook->default_options_wput );
            $options_main = $main_hook->get_option( WPUT_OPTIONS );
        }
        $GLOBALS['WPUT_OPTIONS'] = $options_main;
        /**
         * Include main plugin class
         */
        include_once WPUT_INCLUDES . '/class-wput.php';

        if( class_exists( 'WPUT_Init' ) )
        {
            class WPUT_Load extends WPUT_Init
            {
                function __construct()
                {
                    /**
                     * Main plugin
                     */
                    $this->register_activation_hook( WPUT_FILE, 'activate' );
                    $this->register_deactivation_hook( WPUT_FILE, 'deactivate' );
                    $this->run(); // Call WP_UT_Init function to load and run all plugin components

                    /**
                     * Addons auto-loader class
                     */
                    if( file_exists( WPUT_INCLUDES . '/class-wput-addon-loader.php' ) )
                    {
                        include_once WPUT_INCLUDES . '/class-wput-addon-loader.php';
                        if( class_exists( 'WPUT_Addons_Loader' ) )
                        {
                            new WPUT_Addons_Loader;
                        }
                    }

                    do_action( WPUT_HOOK . 'init' );
                }
            }
        }

        /**
         * Clear CPU and RAM
         */
        $main_hook = NULL;
    }

    /**
     * Everything is probably loaded
     */
    function WPUT()
    {
        if( class_exists( 'WPUT_Load' ) ) return new WPUT_Load;
    }

    /**
     * Let's start the show
     */
    WPUT();
}